/*___Generated_by_IDEA___*/

/*___Generated_by_IDEA___*/

/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Original file: /home/artyom/MyApps/MyMusic/src/com/msoft/android/mplayer/service/IPlay.aidl
 */
package com.msoft.android.mplayer.service;
public interface IPlay extends android.os.IInterface
{
/** Local-side IPC implementation stub class. */
public static abstract class Stub extends android.os.Binder implements com.msoft.android.mplayer.service.IPlay
{
private static final java.lang.String DESCRIPTOR = "com.msoft.android.mplayer.service.IPlay";
/** Construct the stub at attach it to the interface. */
public Stub()
{
this.attachInterface(this, DESCRIPTOR);
}
/**
 * Cast an IBinder object into an com.msoft.android.mplayer.service.IPlay interface,
 * generating a proxy if needed.
 */
public static com.msoft.android.mplayer.service.IPlay asInterface(android.os.IBinder obj)
{
if ((obj==null)) {
return null;
}
android.os.IInterface iin = obj.queryLocalInterface(DESCRIPTOR);
if (((iin!=null)&&(iin instanceof com.msoft.android.mplayer.service.IPlay))) {
return ((com.msoft.android.mplayer.service.IPlay)iin);
}
return new com.msoft.android.mplayer.service.IPlay.Stub.Proxy(obj);
}
@Override public android.os.IBinder asBinder()
{
return this;
}
@Override public boolean onTransact(int code, android.os.Parcel data, android.os.Parcel reply, int flags) throws android.os.RemoteException
{
switch (code)
{
case INTERFACE_TRANSACTION:
{
reply.writeString(DESCRIPTOR);
return true;
}
case TRANSACTION_play:
{
data.enforceInterface(DESCRIPTOR);
int _arg0;
_arg0 = data.readInt();
this.play(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_play_pause:
{
data.enforceInterface(DESCRIPTOR);
this.play_pause();
reply.writeNoException();
return true;
}
case TRANSACTION_next:
{
data.enforceInterface(DESCRIPTOR);
this.next();
reply.writeNoException();
return true;
}
case TRANSACTION_prev:
{
data.enforceInterface(DESCRIPTOR);
this.prev();
reply.writeNoException();
return true;
}
case TRANSACTION_setSongs:
{
data.enforceInterface(DESCRIPTOR);
java.util.List<com.msoft.android.mplayer.lib.models.Song> _arg0;
_arg0 = data.createTypedArrayList(com.msoft.android.mplayer.lib.models.Song.CREATOR);
this.setSongs(_arg0);
reply.writeNoException();
return true;
}
case TRANSACTION_getSongs:
{
data.enforceInterface(DESCRIPTOR);
java.util.List<com.msoft.android.mplayer.lib.models.Song> _result = this.getSongs();
reply.writeNoException();
reply.writeTypedList(_result);
return true;
}
case TRANSACTION_getCurrentSong:
{
data.enforceInterface(DESCRIPTOR);
com.msoft.android.mplayer.lib.models.Song _result = this.getCurrentSong();
reply.writeNoException();
if ((_result!=null)) {
reply.writeInt(1);
_result.writeToParcel(reply, android.os.Parcelable.PARCELABLE_WRITE_RETURN_VALUE);
}
else {
reply.writeInt(0);
}
return true;
}
case TRANSACTION_get3Songs:
{
data.enforceInterface(DESCRIPTOR);
java.lang.String[] _result = this.get3Songs();
reply.writeNoException();
reply.writeStringArray(_result);
return true;
}
case TRANSACTION_isPlaying:
{
data.enforceInterface(DESCRIPTOR);
boolean _result = this.isPlaying();
reply.writeNoException();
reply.writeInt(((_result)?(1):(0)));
return true;
}
case TRANSACTION_getCurrentPlayingPosition:
{
data.enforceInterface(DESCRIPTOR);
int _result = this.getCurrentPlayingPosition();
reply.writeNoException();
reply.writeInt(_result);
return true;
}
}
return super.onTransact(code, data, reply, flags);
}
private static class Proxy implements com.msoft.android.mplayer.service.IPlay
{
private android.os.IBinder mRemote;
Proxy(android.os.IBinder remote)
{
mRemote = remote;
}
@Override public android.os.IBinder asBinder()
{
return mRemote;
}
public java.lang.String getInterfaceDescriptor()
{
return DESCRIPTOR;
}
@Override public void play(int position) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeInt(position);
mRemote.transact(Stub.TRANSACTION_play, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void play_pause() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_play_pause, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void next() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_next, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void prev() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_prev, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public void setSongs(java.util.List<com.msoft.android.mplayer.lib.models.Song> songs) throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
try {
_data.writeInterfaceToken(DESCRIPTOR);
_data.writeTypedList(songs);
mRemote.transact(Stub.TRANSACTION_setSongs, _data, _reply, 0);
_reply.readException();
}
finally {
_reply.recycle();
_data.recycle();
}
}
@Override public java.util.List<com.msoft.android.mplayer.lib.models.Song> getSongs() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
java.util.List<com.msoft.android.mplayer.lib.models.Song> _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getSongs, _data, _reply, 0);
_reply.readException();
_result = _reply.createTypedArrayList(com.msoft.android.mplayer.lib.models.Song.CREATOR);
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
@Override public com.msoft.android.mplayer.lib.models.Song getCurrentSong() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
com.msoft.android.mplayer.lib.models.Song _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getCurrentSong, _data, _reply, 0);
_reply.readException();
if ((0!=_reply.readInt())) {
_result = com.msoft.android.mplayer.lib.models.Song.CREATOR.createFromParcel(_reply);
}
else {
_result = null;
}
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
@Override public java.lang.String[] get3Songs() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
java.lang.String[] _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_get3Songs, _data, _reply, 0);
_reply.readException();
_result = _reply.createStringArray();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
@Override public boolean isPlaying() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
boolean _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_isPlaying, _data, _reply, 0);
_reply.readException();
_result = (0!=_reply.readInt());
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
@Override public int getCurrentPlayingPosition() throws android.os.RemoteException
{
android.os.Parcel _data = android.os.Parcel.obtain();
android.os.Parcel _reply = android.os.Parcel.obtain();
int _result;
try {
_data.writeInterfaceToken(DESCRIPTOR);
mRemote.transact(Stub.TRANSACTION_getCurrentPlayingPosition, _data, _reply, 0);
_reply.readException();
_result = _reply.readInt();
}
finally {
_reply.recycle();
_data.recycle();
}
return _result;
}
}
static final int TRANSACTION_play = (android.os.IBinder.FIRST_CALL_TRANSACTION + 0);
static final int TRANSACTION_play_pause = (android.os.IBinder.FIRST_CALL_TRANSACTION + 1);
static final int TRANSACTION_next = (android.os.IBinder.FIRST_CALL_TRANSACTION + 2);
static final int TRANSACTION_prev = (android.os.IBinder.FIRST_CALL_TRANSACTION + 3);
static final int TRANSACTION_setSongs = (android.os.IBinder.FIRST_CALL_TRANSACTION + 4);
static final int TRANSACTION_getSongs = (android.os.IBinder.FIRST_CALL_TRANSACTION + 5);
static final int TRANSACTION_getCurrentSong = (android.os.IBinder.FIRST_CALL_TRANSACTION + 6);
static final int TRANSACTION_get3Songs = (android.os.IBinder.FIRST_CALL_TRANSACTION + 7);
static final int TRANSACTION_isPlaying = (android.os.IBinder.FIRST_CALL_TRANSACTION + 8);
static final int TRANSACTION_getCurrentPlayingPosition = (android.os.IBinder.FIRST_CALL_TRANSACTION + 9);
}
public void play(int position) throws android.os.RemoteException;
public void play_pause() throws android.os.RemoteException;
public void next() throws android.os.RemoteException;
public void prev() throws android.os.RemoteException;
public void setSongs(java.util.List<com.msoft.android.mplayer.lib.models.Song> songs) throws android.os.RemoteException;
public java.util.List<com.msoft.android.mplayer.lib.models.Song> getSongs() throws android.os.RemoteException;
public com.msoft.android.mplayer.lib.models.Song getCurrentSong() throws android.os.RemoteException;
public java.lang.String[] get3Songs() throws android.os.RemoteException;
public boolean isPlaying() throws android.os.RemoteException;
public int getCurrentPlayingPosition() throws android.os.RemoteException;
}
