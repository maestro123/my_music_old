package com.example.ImageTest.utils;

import android.support.v4.app.FragmentActivity;

import java.util.HashMap;
import java.util.Set;

/**
 * Created by artyom on 9/1/14.
 */
public class MBActivity extends FragmentActivity {

    public interface OnBackPressedListener {
        public boolean onBackPressed();
    }

    private HashMap<Object, OnBackPressedListener> mListeners = new HashMap<Object, OnBackPressedListener>();

    public void attachOnBackPressedListener(Object key, OnBackPressedListener listener) {
        if (mListeners.containsKey(key))
            mListeners.remove(key);
        mListeners.put(key, listener);
    }

    public void detachListener(Object key) {
        mListeners.remove(key);
    }

    @Override
    public void onBackPressed() {
        if (!anyHoldBack())
            super.onBackPressed();
    }


    private boolean anyHoldBack() {
        final int size = mListeners.size();
        Object[] keys = mListeners.keySet().toArray(new Object[size]);
        for (int i = size - 1; i > -1; i--) {
            if (mListeners.get(keys[i]).onBackPressed())
                return true;
        }
        return false;
    }
}
