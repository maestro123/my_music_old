package com.example.ImageTest.utils;

import android.app.Activity;
import android.support.v4.app.DialogFragment;

/**
 * Created by artyom on 9/1/14.
 */
public abstract class MBFragment extends DialogFragment implements MBActivity.OnBackPressedListener {

    private MBActivity mbActivity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof MBActivity) {
            mbActivity = (MBActivity) activity;
            mbActivity.attachOnBackPressedListener(getKey(), this);
        }
    }

    @Override
    public void onDetach() {
        if (mbActivity != null)
            mbActivity.detachListener(getKey());
        super.onDetach();
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }

    public abstract Object getKey();

}
