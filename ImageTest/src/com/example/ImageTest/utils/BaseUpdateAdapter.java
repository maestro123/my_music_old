package com.example.ImageTest.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;

/**
 * Created by artyom on 9/1/14.
 */
public abstract class BaseUpdateAdapter<T> extends BaseAdapter {

    private T[] items;

    public LayoutInflater mInflater;

    public BaseUpdateAdapter(Context context) {
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void update(T[] items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public T[] getItems() {
        return items;
    }

    @Override
    public int getCount() {
        return items != null ? items.length : 0;
    }

    @Override
    public T getItem(int i) {
        return items[i];
    }
}
