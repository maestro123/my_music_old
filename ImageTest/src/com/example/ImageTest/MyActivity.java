package com.example.ImageTest;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import com.example.ImageTest.ui.ImageFragment;
import com.example.ImageTest.ui.VideoFragment;
import com.example.ImageTest.utils.MBActivity;

public class MyActivity extends MBActivity {

    private ViewPager mPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new FragmentsAdapter(getSupportFragmentManager()));
    }

    private final class FragmentsAdapter extends FragmentPagerAdapter {

        public FragmentsAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 1:
                    return new ImageFragment();
                default:
                    return new VideoFragment();
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

}
