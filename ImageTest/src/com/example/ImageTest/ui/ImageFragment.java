package com.example.ImageTest.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.dream.android.mim.MIM2;
import com.dream.android.mim.MIMDefaultMaker;
import com.dream.android.mim.RecyclingImageView;
import com.example.ImageTest.R;
import com.example.ImageTest.utils.BaseUpdateAdapter;
import com.example.ImageTest.utils.MBFragment;
import com.msoft.android.mplayer.lib.MediaStoreHelper;
import com.msoft.android.mplayer.lib.models.BaseMediaItem;
import com.msoft.android.mplayer.lib.models.Image;
import com.msoft.android.mplayer.lib.models.MediaBucket;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by artyom on 9/1/14.
 */
public class ImageFragment extends MBFragment implements LoaderManager.LoaderCallbacks<BaseMediaItem[]>,
        AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    public static final String TAG = ImageFragment.class.getSimpleName();

    private ArrayList<MediaBucket> mBuckets = new ArrayList<MediaBucket>();
    private GridView mGrid;
    private ImagesAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.grid_view, null);
        mGrid = (GridView) v.findViewById(R.id.list);
        mGrid.setOnItemClickListener(this);
        mGrid.setOnItemLongClickListener(this);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mGrid.setAdapter(mAdapter = new ImagesAdapter(getActivity()));
        load();
    }

    private void load() {
        if (getLoaderManager().getLoader(TAG.hashCode()) != null) {
            getLoaderManager().restartLoader(TAG.hashCode(), null, this);
        } else {
            getLoaderManager().initLoader(TAG.hashCode(), null, this);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Object item = adapterView.getItemAtPosition(i);
        if (item instanceof MediaBucket) {
            mBuckets.add((MediaBucket) item);
            load();
        } else {
            //TODO: open preview fragment
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        return false;
    }

    @Override
    public Object getKey() {
        return TAG;
    }

    @Override
    public Loader<BaseMediaItem[]> onCreateLoader(int i, Bundle bundle) {
        return new ImagesLoader(getActivity(),
                mBuckets.size() == 0 ? null : mBuckets.get(mBuckets.size() - 1).Id);
    }

    @Override
    public void onLoadFinished(Loader<BaseMediaItem[]> loader, BaseMediaItem[] baseMediaItems) {
        if (mAdapter != null) {
            mAdapter.update(baseMediaItems);
        }
    }

    @Override
    public void onLoaderReset(Loader<BaseMediaItem[]> loader) {
    }

    @Override
    public boolean onBackPressed() {
        if (mBuckets.size() > 0) {
            mBuckets.remove(mBuckets.size() - 1);
            load();
            return true;
        }
        return super.onBackPressed();
    }

    private final class ImagesAdapter extends BaseUpdateAdapter<BaseMediaItem> {

        private int size;
        private MIM2 mim;

        public ImagesAdapter(Context context) {
            super(context);
            size = context.getResources().getDisplayMetrics().widthPixels / 3;
            mim = new MIM2(context).setMaker(new MIMDefaultMaker()).setDefaultSize(size, size);
        }

        @Override
        public long getItemId(int i) {
            return getItem(i).Id;
        }

        @Override
        public View getView(int position, View v, ViewGroup viewGroup) {
            Holder h;
            if (v == null) {
                h = new Holder();
                v = mInflater.inflate(R.layout.image_item_vuew, null);
                h.Image = (RecyclingImageView) v.findViewById(R.id.image);
                h.Title = (TextView) v.findViewById(R.id.title);
                h.Image.setLayoutParams(new RelativeLayout.LayoutParams(size, size));
                v.setTag(h);
            } else {
                h = (Holder) v.getTag();
            }
            BaseMediaItem image = getItem(position);
            if (image instanceof Image) {
                h.Title.setVisibility(View.GONE);
                MIM2.ImageLoadObject loadObject = new MIM2.ImageLoadObject(image.Path, image.Path);
                mim.loadImage(loadObject, h.Image);
            } else {
                h.Title.setVisibility(View.VISIBLE);
                h.Title.setText(image.Title);
            }
            return v;
        }

        class Holder {
            TextView Title;
            ImageView Image;
        }

    }

    public static final class ImagesLoader extends AsyncTaskLoader<BaseMediaItem[]> {

        public static final int ALL_IMAGES = -2;

        private Long parentId;

        public ImagesLoader(Context context, Long id) {
            super(context);
            parentId = id;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public BaseMediaItem[] loadInBackground() {
            if (parentId != null) {
                List<Image> out = MediaStoreHelper.getImages(getContext(), parentId != ALL_IMAGES ? parentId : null);
                return out != null ? out.toArray(new Image[out.size()]) : null;
            } else {
                List<MediaBucket> out = MediaStoreHelper.getImageBuckets(getContext());
                return out != null ? out.toArray(new MediaBucket[out.size()]) : null;
            }
        }
    }

}
