package maestro.mymusic.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

/**
 * Created by Artyom on 06.08.2014.
 */
public class DirectionListView extends ListView {

    public static final String TAG = DirectionListView.class.getSimpleName();

    private int mInitialOffset;
    private int mOverScrollHeight = 0;
    private boolean isScrollToBottom;

    private OnOverScrollListener mOverScrollListener;
    private DirectionListDisplayer mDisplayer;

    public interface DirectionListDisplayer {
        public void animate(View view, int position);
    }

    public interface OnOverScrollListener {
        public void onOverScroll(int scrollX, int scrollY, boolean clampedX, boolean clampedY);
    }

    public DirectionListView(Context context) {
        super(context);
    }

    public DirectionListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DirectionListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected int computeVerticalScrollOffset() {
        final int offset = super.computeVerticalScrollOffset();
        if (Math.abs(mInitialOffset - offset) > 5) {
            isScrollToBottom = mInitialOffset <= offset;
            mInitialOffset = offset;
        }
        return offset;
    }

    public boolean isScrollToBottom() {
        return isScrollToBottom;
    }

    public void setOnOverScrollListener(OnOverScrollListener listener) {
        mOverScrollListener = listener;
    }

    public void setDisplayer(DirectionListDisplayer displayer) {
        mDisplayer = displayer;
    }

    public void setOverScrollHeight(int height) {
        mOverScrollHeight = height;
    }

    @Override
    protected void onOverScrolled(int scrollX, int scrollY, boolean clampedX, boolean clampedY) {
        super.onOverScrolled(scrollX, scrollY, clampedX, clampedY);
        if (mOverScrollListener != null) {
            mOverScrollListener.onOverScroll(scrollX, scrollY, clampedX, clampedY);
        }
    }

    @Override
    protected boolean overScrollBy(int deltaX, int deltaY, int scrollX, int scrollY, int scrollRangeX, int scrollRangeY, int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent) {
        return super.overScrollBy(deltaX, deltaY, scrollX, scrollY, scrollRangeX, scrollRangeY, maxOverScrollX, mOverScrollHeight, isTouchEvent);
    }

}
