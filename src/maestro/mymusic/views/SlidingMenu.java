package maestro.mymusic.views;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.ViewConfigurationCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.*;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;
import android.widget.Scroller;

/**
 * Created by artyom on 18.9.14.
 */
public class SlidingMenu extends LinearLayout {

    public static final String TAG = SlidingMenu.class.getSimpleName();

    private View contentView;

    private int shadowAlpha = (int) (255 * 0.7f);
    private int contentWidth;
    private int touchWidth;
    private int mTouchSlop;
    private boolean isMenuOpen;
    private boolean isEnable = true;
    private GestureDetectorCompat mGestureDetector;
    private AnimationRunnable mRunnable;

    // 0 - none, 1 - left, 2 - right
    public int mFlingState = 0;

    public interface OnMenuActionListener {

        public void onMenuSlide();

        public void onMenuClosed();

        public void onMenuOpened();

    }

    public SlidingMenu(Context context) {
        super(context);
        init();
    }

    public SlidingMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SlidingMenu(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void setContentView(View view) {
        addView(contentView = view, 0, new LayoutParams(contentWidth, ViewGroup.LayoutParams.MATCH_PARENT));
        contentView.setTranslationX(-contentWidth);
        mRunnable = new AnimationRunnable(contentView);
    }

    void init() {
        contentWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 300,
                getResources().getDisplayMetrics());
//        touchWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30,
//                getResources().getDisplayMetrics());
        final ViewConfiguration configuration = ViewConfiguration.get(getContext());
        mTouchSlop = touchWidth = ViewConfigurationCompat.getScaledPagingTouchSlop(configuration);
        mGestureDetector = new GestureDetectorCompat(getContext(), mGestureListener);
    }

    private boolean onSlide;
    private boolean localSlideEnable;
    private float prevX;
    private float startX;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mGestureDetector.onTouchEvent(event);
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (mRunnable != null) {
                    mRunnable.finish();
                }
                mFlingState = 0;
                startX = event.getRawX();
                localSlideEnable = event.getRawX() <= (isMenuOpen ? getWidth() + contentWidth : touchWidth);
                if (localSlideEnable)
                    return true;
                break;
            case MotionEvent.ACTION_MOVE:
                if (!onSlide && localSlideEnable) {
                    boolean localOnSlide = isMenuOpen || (Math.abs(event.getRawX() - startX) > mTouchSlop
                            && (isEnable && contentView != null));
                    if (localOnSlide) {
                        onSlide = localOnSlide;
                        prevX = event.getRawX();
                        return true;
                    }
                }
                if (onSlide) {
                    float x = Math.max(-contentWidth, Math.min(contentView.getTranslationX()
                            + (event.getRawX() - prevX), 0));
                    contentView.setTranslationX(x);
                    prevX = event.getRawX();
                    onMenuSlide();
                    return true;
                }
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                if (onSlide) {
                    if (mFlingState == 2) {
                        openMenu();
                    } else if (mFlingState == 1) {
                        closeMenu();
                    } else if (Math.abs(contentView.getTranslationX()) < contentWidth / 2) {
                        openMenu();
                    } else {
                        closeMenu();
                    }
                    onSlide = false;
                    return true;
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    public void openMenu() {
        isMenuOpen = true;
        contentView.animate().setDuration(150).translationX(0).start();
    }

    public void closeMenu() {
        isMenuOpen = false;
        contentView.animate().setDuration(150).translationX(-contentWidth).start();
    }

    public boolean isOpened() {
        return isMenuOpen;
    }

    private float getSlidePercent(float total, float current) {
        return current != 0 ? 1f - Math.abs(current / total) : 1;
    }

    private void onMenuSlide() {
//        float somePercent = getSlidePercent(contentWidth, contentView.getTranslationX());
//        setBackgroundColor(Color.argb((int) (somePercent * 127), 0, 0, 0));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        Log.e(TAG, "sliding menu size = " + getWidth() + "/" + getHeight());
    }

    public void setMenuEnable(boolean enable) {
        isEnable = enable;
    }

    public void setShadowAlpha(float value) {
        shadowAlpha = (int) (255 * value);
    }

    private GestureDetector.SimpleOnGestureListener mGestureListener = new GestureDetector.SimpleOnGestureListener() {

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            mFlingState = e1.getRawX() < e2.getRawX() ? 2 : 1;
            return super.onFling(e1, e2, velocityX, velocityY);
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            if (isMenuOpen && e.getRawX() > contentWidth) {
                closeMenu();
                return true;
            }
            return super.onSingleTapConfirmed(e);
        }
    };

    private class AnimationRunnable implements Runnable {

        private Interpolator interpolator = new DecelerateInterpolator(3);

        private View mView;
        private boolean isRunning;
        private boolean forceFinish;
        private int target;
        private int step;

        public AnimationRunnable(View view) {
            mView = view;
        }

        public void start(int initialVelocity, int x) {
            target = x;
            step = (int) (Math.abs(mView.getTranslationX()) - x) / 300;
        }

        public boolean isRunning() {
            return false;
        }

        public void finish() {
            forceFinish = true;
        }

        @Override
        public void run() {
            if (forceFinish || !isRunning) {
                forceFinish = false;
                isRunning = false;
                return;
            }


        }
    }

}
