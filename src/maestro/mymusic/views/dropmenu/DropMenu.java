package maestro.mymusic.views.dropmenu;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Xml;
import android.view.View;
import org.xmlpull.v1.XmlPullParser;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by artyom on 9/6/14.
 */
public class DropMenu extends View {

    public static int DIRECTION_TOP = 0;
    public static int DIRECTON_BOTTOM = 1;

    private int mDirection = DIRECTION_TOP;

    private int itemPadding;

    private ArrayList<DropMenuItem> mItems = new ArrayList<DropMenuItem>();

    public DropMenu(Context context) {
        super(context);
    }

    public DropMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DropMenu(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int totalWidth = getBiggestWidth();
        int totalHeight = getItemsTotalHeight();
        setMeasuredDimension(totalWidth, totalHeight);

    }

    private int getItemsTotalHeight() {
        int height = itemPadding;
        for (DropMenuItem item : mItems)
            height += item.Height + item.Height;
        return height;
    }

    private int getBiggestWidth() {
        int width = 0;
        for (DropMenuItem item : mItems)
            if (item.Width > width)
                width = item.Width;
        return width;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

    }

    public static class DropMenuItem {

        public int Id;
        public String Title;
        public int IconResource;
        public int BackgroundResource;
        public int Width;
        public int Height;

        public DropMenuItem(int id, String title, int iconResource, int backgroundResource) {
            Id = id;
            Title = title;
            IconResource = iconResource;
            BackgroundResource = backgroundResource;
        }

        public DropMenuItem setSize(int width, int height) {
            Width = width;
            Height = height;
            return this;
        }

    }

    private void parseXML(int id) {
        try {
            InputStream stream = getResources().openRawResource(id);
            try {
                XmlPullParser parser = Xml.newPullParser();
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                parser.setInput(stream, null);
                parser.nextTag();

                while (parser.next() != XmlPullParser.END_TAG) {
                    if (parser.getEventType() != XmlPullParser.START_TAG) {
                        continue;
                    }
                    String name = parser.getName();
                    // Starts by looking for the entry tag
//                    if (name.equals("entry")) {
//                        entries.add(readEntry(parser));
//                    } else {
//                        skip(parser);
//                    }
                }

            } finally {
                stream.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
