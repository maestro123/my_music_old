package maestro.mymusic.views.dropmenu;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.AnticipateOvershootInterpolator;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import java.util.ArrayList;

/**
 * Created by artyom on 9/6/14.
 */
public class BDropMenu extends LinearLayout {

    public static final String TAG = BDropMenu.class.getSimpleName();

    private ArrayList<DropMenu.DropMenuItem> menuItems = new ArrayList<DropMenu.DropMenuItem>();
    private boolean onAnimate;

    public BDropMenu(Context context) {
        super(context);
        init();
    }

    public BDropMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BDropMenu(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    void init() {
        setOrientation(VERTICAL);
        setVisibility(GONE);
    }

    public void addItem(DropMenu.DropMenuItem item) {
        menuItems.add(item);
    }

    public void prepare() {
        for (DropMenu.DropMenuItem item : menuItems)
            addItemView(item);
        hide();
    }

    private void addItemView(DropMenu.DropMenuItem item) {
        ImageButton view = new ImageButton(getContext());
        view.setImageResource(item.IconResource);
        view.setBackgroundResource(item.BackgroundResource);
        LinearLayout.LayoutParams params = new LayoutParams(item.Width, item.Height);
        params.bottomMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, getResources().getDisplayMetrics());
        addView(view, params);
    }

    public void show() {
        if (!onAnimate) {
            onAnimate = true;
            setVisibility(View.VISIBLE);
            final int childCount = getChildCount();
            final int translate = getBottom() - getTop();
            for (int i = 0; i < childCount; i++) {
                View v = getChildAt(i);
                v.setY(-translate);
                animateView(v, 0, 1f, 80 * i, translate, 0, i == childCount - 1 ? new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        onAnimate = false;
                    }
                } : null);
            }
        }
    }

    public void hide() {
        if (!onAnimate) {
            onAnimate = true;
            final int childCount = getChildCount() - 1;
            final int translate = getBottom() - getTop();
            for (int i = childCount; i > -1; i--) {
                View v = getChildAt(i);
                animateView(v, 1f, 0f, 80 * (childCount - i), 0, translate, i == 0 ? new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        setVisibility(GONE);
                        onAnimate = false;
                    }
                } : null);

            }
        }
    }

    private void animateView(View view, float fromAlpha, float toAlpha, int delay, float fromX, float toX, AnimatorListenerAdapter mListener) {
        AnimatorSet set = new AnimatorSet();
        set.setStartDelay(delay);
        set.setDuration(350);
        set.setInterpolator(new AnticipateOvershootInterpolator(1f));
        ObjectAnimator aAnim = ObjectAnimator.ofFloat(view, "alpha", fromAlpha, toAlpha);
        ObjectAnimator tAnim = ObjectAnimator.ofFloat(view, "translationY", fromX, toX);
        set.playTogether(aAnim, tAnim);
        if (mListener != null)
            set.addListener(mListener);
        set.start();
    }

    public void bindClickView(View view) {
        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getVisibility() == VISIBLE)
                    hide();
                else
                    show();
            }
        });
    }

}
