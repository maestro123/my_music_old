package maestro.mymusic.views.dropmenu;

import android.content.res.Resources;
import android.util.Log;
import android.util.Xml;
import org.xmlpull.v1.XmlPullParser;

import java.io.InputStream;

/**
 * Created by artyom on 9/6/14.
 */
public class DoppMenuParser {

    public static final String TAG = DoppMenuParser.class.getSimpleName();

    private void parseXML(Resources resources, int id) {
        try {
            InputStream stream = resources.openRawResource(id);
            try {
                XmlPullParser parser = Xml.newPullParser();
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                parser.setInput(stream, null);
                parser.nextTag();

                while (parser.next() != XmlPullParser.END_TAG) {
                    if (parser.getEventType() != XmlPullParser.START_TAG) {
                        continue;
                    }
                    String name = parser.getName();
                    Log.e(TAG, "name = " + name);
                }

            } finally {
                stream.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
