package maestro.mymusic.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Bitmap;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import com.dream.android.mim.MIMAbstractDisplayer;
import com.dream.android.mim.RecyclingBitmapDrawable;

import java.util.Random;

/**
 * Created by artyom on 8/17/14.
 */
public class MFlipDisplayer extends MIMAbstractDisplayer {
    @Override
    public void display(ImageView imageView) {
        Interpolator interpolator = new DecelerateInterpolator(2);
        AnimatorSet set = new AnimatorSet();
        ObjectAnimator back = ObjectAnimator.ofFloat(imageView, "rotationY", 270f, 360f);
        ObjectAnimator syAnim = ObjectAnimator.ofFloat(imageView, "scaleY", 0.4f, 1f);
        ObjectAnimator sxAnim = ObjectAnimator.ofFloat(imageView, "scaleX", 0.4f, 1f);
        set.playTogether(back, sxAnim, syAnim);
        set.setDuration(animationDuration);
        set.setInterpolator(interpolator);
        set.start();
    }

    public void display(View imageView) {
        Interpolator interpolator = new DecelerateInterpolator(2);
        AnimatorSet set = new AnimatorSet();
        ObjectAnimator back = ObjectAnimator.ofFloat(imageView, "rotationY", 270f, 360f);
        ObjectAnimator syAnim = ObjectAnimator.ofFloat(imageView, "scaleY", 0.4f, 1f);
        ObjectAnimator sxAnim = ObjectAnimator.ofFloat(imageView, "scaleX", 0.4f, 1f);
        set.playTogether(back, sxAnim, syAnim);
        set.setDuration(animationDuration);
        set.setInterpolator(interpolator);
        set.start();
    }

    private final int startDelay = 100;
    private final int animationDuration = 600;

    public void animateIn(final ImageView imageView, final Object bitmapObject, int position) {
        AnimatorSet set = new AnimatorSet();
        Interpolator interpolator = new DecelerateInterpolator(2);
        ObjectAnimator animation = ObjectAnimator.ofFloat(imageView, "rotationY", 0.0f, 90f);
        ObjectAnimator syAnim = ObjectAnimator.ofFloat(imageView, "scaleY", 1f, 0.4f);
        ObjectAnimator sxAnim = ObjectAnimator.ofFloat(imageView, "scaleX", 1f, 0.4f);
        set.playTogether(syAnim, sxAnim, animation);
        set.setStartDelay(getRandomStartDelay());
        set.setDuration(animationDuration);
        set.setInterpolator(interpolator);
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                if (bitmapObject instanceof RecyclingBitmapDrawable) {
                    imageView.setImageDrawable((RecyclingBitmapDrawable) bitmapObject);
                } else if (bitmapObject instanceof Bitmap) {
                    imageView.setImageBitmap((Bitmap) bitmapObject);
                }
                imageView.setBackgroundDrawable(null);
                Interpolator interpolator = new DecelerateInterpolator(2);
                AnimatorSet set = new AnimatorSet();
                ObjectAnimator back = ObjectAnimator.ofFloat(imageView, "rotationY", 270f, 360f);
                ObjectAnimator syAnim = ObjectAnimator.ofFloat(imageView, "scaleY", 0.4f, 1f);
                ObjectAnimator sxAnim = ObjectAnimator.ofFloat(imageView, "scaleX", 0.4f, 1f);
                set.playTogether(back, sxAnim, syAnim);
                set.setDuration(animationDuration);
                set.setInterpolator(interpolator);
                set.start();
            }
        });
        set.start();
    }

    private int MAX_DELAY = 600;
    private int MIN_DELAY = 200;

    final Random r = new Random();

    private int getRandomDelay() {
        return (r.nextInt(MAX_DELAY - MIN_DELAY + 1) + MIN_DELAY) / 100 * 100;
    }

    private int getRandomStartDelay() {
        return (r.nextInt(4 - 1 + 1) + 1) * 100 * 2;
    }

}
