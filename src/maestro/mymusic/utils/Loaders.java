package maestro.mymusic.utils;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import com.msoft.android.mplayer.lib.MediaStoreHelper;
import com.msoft.android.mplayer.lib.models.*;
import maestro.mymusic.R;
import maestro.mymusic.utils.adapters.UniqRowAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by artyom on 7/10/14.
 */
public class Loaders {

    public static class AlbumLoader extends AsyncTaskLoader<Album[]> {

        public AlbumLoader(Context context) {
            super(context);
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public Album[] loadInBackground() {
            List<Album> albums = MediaStoreHelper.getAlbums(getContext(), null);
            return albums != null ? albums.toArray(new Album[albums.size()]) : null;
        }
    }

    public static class SongsLoader extends AsyncTaskLoader<Song[]> {

        private BaseMediaItem mItem;

        public SongsLoader(Context context, BaseMediaItem item) {
            super(context);
            this.mItem = item;
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public Song[] loadInBackground() {
            List<Song> songs = null;
            if (mItem == null) {
                songs = MediaStoreHelper.getSongs(getContext(), null);
            } else if (mItem instanceof Album) {
                songs = MediaStoreHelper.getSongs(getContext(), ((Album) mItem).Id);
            } else if (mItem instanceof Genre) {
                songs = MediaStoreHelper.getSongsByGenre(getContext(), ((Genre) mItem).Id);
            } else if (mItem instanceof Artist) {
                songs = MediaStoreHelper.getSongsByArtist(getContext(), ((Artist) mItem).Id);
            } else if (mItem instanceof Playlist) {
                songs = MediaStoreHelper.getSongsByPlaylist(getContext(), ((Playlist) mItem).Id);
            }
            return songs != null ? songs.toArray(new Song[songs.size()]) : null;
        }
    }

    public static class GenresLoader extends AsyncTaskLoader<Genre[]> {

        public GenresLoader(Context context) {
            super(context);
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public Genre[] loadInBackground() {
            List<Genre> genres = MediaStoreHelper.getGenres(getContext());
            return genres != null ? genres.toArray(new Genre[genres.size()]) : null;
        }
    }

    public static class ArtistLoader extends AsyncTaskLoader<Artist[]> {

        public ArtistLoader(Context context) {
            super(context);
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public Artist[] loadInBackground() {
            List<Artist> genres = MediaStoreHelper.getArtists(getContext());
            return genres != null ? genres.toArray(new Artist[genres.size()]) : null;
        }
    }

    public static class PlaylistsLoader extends AsyncTaskLoader<Playlist[]> {

        public PlaylistsLoader(Context context) {
            super(context);
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public Playlist[] loadInBackground() {
            List<Playlist> playlists = MediaStoreHelper.getPlaylists(getContext());
            return playlists != null ? playlists.toArray(new Playlist[playlists.size()]) : null;
        }

    }

    public static class GlobalSearchLoader extends AsyncTaskLoader<BaseMediaItem[]> {

        private String[] mHeader;
        private String query;

        public GlobalSearchLoader(Context context, String query) {
            super(context);
            this.query = query;
            mHeader = getContext().getResources().getStringArray(R.array.search_headers);
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            forceLoad();
        }

        @Override
        public BaseMediaItem[] loadInBackground() {
            List<Album> albums = MediaStoreHelper.searchAlbum(getContext(), query);
            List<Song> songs = MediaStoreHelper.searchSongs(getContext(), query);
            List<Artist> artists = MediaStoreHelper.searchArtist(getContext(), query);
            List<Genre> genres = MediaStoreHelper.searchGenre(getContext(), query);
            List<BaseMediaItem> items = new ArrayList<BaseMediaItem>();
            items.add(new UniqRowAdapter.HeaderItem(mHeader[0]));
            items.addAll(albums);
            items.add(new UniqRowAdapter.HeaderItem(mHeader[1]));
            items.addAll(songs);
            items.add(new UniqRowAdapter.HeaderItem(mHeader[2]));
            items.addAll(artists);
            return items.toArray(new BaseMediaItem[items.size()]);
        }
    }

}
