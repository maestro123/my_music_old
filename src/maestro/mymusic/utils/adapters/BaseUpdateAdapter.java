package maestro.mymusic.utils.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import maestro.mymusic.R;

/**
 * Created by artyom on 7/10/14.
 */
public abstract class BaseUpdateAdapter<T> extends BaseAdapter implements AbsListView.OnScrollListener {

    public static final int DEF_PADDING = -1;

    private T[] mItems;

    public LayoutInflater mInflater;
    public Context mContext;
    public int TopPadding;

    public BaseUpdateAdapter(Context context) {
        this(context, DEF_PADDING);
    }

    public BaseUpdateAdapter(Context context, int topPadding) {
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TopPadding = topPadding != DEF_PADDING ? topPadding
                : context.getResources().getDimensionPixelOffset(R.dimen.actionbar_height);
    }

    @Override
    public int getCount() {
        return mItems != null ? mItems.length : 0;
    }

    @Override
    public T getItem(int i) {
        return mItems[i];
    }

    public void update(T[] items) {
        mItems = items;
        notifyDataSetChanged();
    }

    public T[] getItems() {
        return mItems;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

    }
}
