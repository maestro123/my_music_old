package maestro.mymusic.utils.adapters;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import maestro.mymusic.R;

/**
 * Created by Artyom on 24.06.2014.
 */
public abstract class MGridAdapter<T> extends BaseUpdateAdapter<T> implements View.OnClickListener, View.OnLongClickListener {

    public static final String TAG = MGridAdapter.class.getSimpleName();

    private OnItemClickListener mClickListener;

    private int bigSize;
    private int smallSize;
    private int spacing = 2;
    private int totalWidth;

    public interface OnItemClickListener {
        public void onItemClick(View v, int position);

        public boolean onItemLongClick(View v, int position);
    }

    public MGridAdapter(Context context, int totalWidth, int topPadding) {
        super(context, topPadding);
        this.totalWidth = totalWidth;
        initSize();
    }

    public void initSize() {
        final DisplayMetrics metrics = mContext.getResources().getDisplayMetrics();
        final int screenWidth = totalWidth;//metrics.widthPixels;
        final int screenHeight = metrics.heightPixels;
        final int dpi = metrics.densityDpi;
        final int thirdPart = screenWidth / 3;
        bigSize = thirdPart * 2;
        smallSize = thirdPart;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mClickListener = listener;
    }

    @Override
    public int getCount() {
        int originalCount = super.getCount();
        int outCount = originalCount / 3;
        if (outCount != 0 && outCount * 3 < originalCount)
            outCount++;
        return outCount;
    }

    public int getOriginCount() {
        return getItems() != null ? getItems().length : 0;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = new FrameLayout(mContext);
        }
        FrameLayout.LayoutParams[] params = new FrameLayout.LayoutParams[3];
        if (position % 3 == 0) {
            params[0] = new FrameLayout.LayoutParams(bigSize, bigSize);
            params[1] = new FrameLayout.LayoutParams(smallSize - spacing, smallSize - spacing / 2);
            params[2] = new FrameLayout.LayoutParams(smallSize - spacing, smallSize - spacing / 2);
            params[1].leftMargin = bigSize + spacing;
            params[2].leftMargin = bigSize + spacing;
            params[2].topMargin = smallSize + spacing / 2;
        } else if (position % 2 == 0) {
            params[0] = new FrameLayout.LayoutParams(smallSize - spacing / 2, smallSize);
            params[1] = new FrameLayout.LayoutParams(smallSize - spacing / 2, smallSize);
            params[1].leftMargin = smallSize + spacing / 2;
            params[2] = new FrameLayout.LayoutParams(smallSize - spacing, smallSize);
            params[2].leftMargin = bigSize + spacing;
        } else {
            params[0] = new FrameLayout.LayoutParams(smallSize - spacing / 2, smallSize - spacing / 2);
            params[1] = new FrameLayout.LayoutParams(smallSize - spacing / 2, smallSize - spacing / 2);
            params[2] = new FrameLayout.LayoutParams(bigSize, bigSize);
            params[2].leftMargin = params[1].topMargin = smallSize + spacing / 2;
        }

        convertView.setPadding(convertView.getPaddingLeft(), position == 0 ? TopPadding : 0,
                convertView.getPaddingRight(), 2);//convertView.getPaddingBottom() + 2);
        int startPosition = position * 3;
        if (((FrameLayout) convertView).getChildCount() == 3) {
            for (int i = 0; i < 3; i++) {
                int ap = startPosition + i;
                View v = ((FrameLayout) convertView).getChildAt(i);
                if (ap > getOriginCount() - 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.setVisibility(View.VISIBLE);
                    FrameLayout.LayoutParams lParams = params[i];
                    v.setLayoutParams(lParams);
                    instantiateItem(v, ap, lParams.width, lParams.height);
                    v.setTag(R.id.spec_tag, ap);
                    v.setOnClickListener(this);
                    v.setOnLongClickListener(this);
                }
            }
        } else {
            ((FrameLayout) convertView).removeAllViews();
            for (int i = 0; i < 3; i++) {
                int ap = startPosition + i;
                if (ap > getOriginCount() - 1) break;
                FrameLayout.LayoutParams lParams = params[i];
                View v = instantiateItem(null, ap, lParams.width, lParams.height);
                v.setLayoutParams(lParams);
                ((FrameLayout) convertView).addView(v);
                v.setTag(R.id.spec_tag, ap);
                v.setOnClickListener(this);
                v.setOnLongClickListener(this);
            }
        }
        return convertView;
    }

    public abstract View instantiateItem(View v, int position, int width, int height);

    @Override
    public void onClick(View v) {
        if (mClickListener != null) {
            mClickListener.onItemClick(v, (Integer) v.getTag(R.id.spec_tag));
        }
    }

    @Override
    public boolean onLongClick(View v) {
        return mClickListener != null ? mClickListener.onItemLongClick(v, (Integer) v.getTag(R.id.spec_tag)) : false;
    }

    public int getRowHeaderCountFromPosition(int position) {
        final int count = getCount();
        int out = 0;
        for (int i = 0; i < count; i++) {
            if (getItem(i) instanceof RowHeader) {
                out++;
            }
        }
        return out;
    }

    public interface RowHeader {

    }

}
