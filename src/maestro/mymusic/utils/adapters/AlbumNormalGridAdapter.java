package maestro.mymusic.utils.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.dream.android.mim.MIM2;
import com.dream.android.mim.MIMManager;
import com.msoft.android.mplayer.lib.models.Album;
import maestro.mymusic.Constants;
import maestro.mymusic.R;
import maestro.mymusic.ui.AlbumCoverFetchDialog;
import maestro.mymusic.utils.AlbumCoverMaker;
import maestro.mymusic.utils.MFlipDisplayer;
import maestro.mymusic.utils.Typefacer;

import java.util.Random;

/**
 * Created by artyom on 8/29/14.
 */
public class AlbumNormalGridAdapter extends BaseUpdateAdapter<Album> implements View.OnClickListener,
        View.OnLongClickListener {

    private MIM2 mim;
    private MFlipDisplayer mFlipDisplayer = new MFlipDisplayer();
    private int width, height, numOfColumns = 1, padding, imageWidth, imageHeight;
    private MGridAdapter.OnItemClickListener mItemClickListener;

    public AlbumNormalGridAdapter(Context context, int totalWidth, int numOfColumns, int topPadding) {
        super(context, topPadding);
        this.numOfColumns = numOfColumns;
        mim = MIMManager.getInstance().getMIM(Constants.ALBUM_COVER_MIM);
        if (mim == null) {
            mim = new MIM2(context.getApplicationContext())
                    .setMaker(new AlbumCoverMaker(context.getApplicationContext()));
            MIMManager.getInstance().addMIM(Constants.ALBUM_COVER_MIM, mim);
        }
        height = width = totalWidth / numOfColumns;
        padding = context.getResources().getDimensionPixelSize(R.dimen.album_item_grid_view_padding);
        imageHeight = imageWidth = width - padding * 2;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public void setOnItemClickListener(MGridAdapter.OnItemClickListener listener) {
        mItemClickListener = listener;
    }

    @Override
    public int getCount() {
        int realCount = getItems() != null ? getItems().length : 0;
        if (realCount == 0) return 0;
        int outCount = realCount / numOfColumns;
        return outCount == 0 ? 1 : outCount % numOfColumns > 0 ? outCount + 1 : outCount;
    }

    @Override
    public View getView(int position, View v, ViewGroup viewGroup) {
        FrameLayout layout = (FrameLayout) v;
        if (layout == null) {
            v = layout = new FrameLayout(mContext);
        }
        v.setPadding(0, position == 0 ? TopPadding : 0, 0, 0);
        int realPosition = position * numOfColumns;
        int leftItems = getCount() - realPosition;
        int drawItemsCount = leftItems < numOfColumns ? leftItems : numOfColumns;
        int childCount = layout.getChildCount();
        if (childCount != drawItemsCount) {
            layout.removeAllViews();
        }
        for (int i = 0; i < drawItemsCount; i++) {
            View view = i < childCount ? layout.getChildAt(i) : null;
            if (view != null) {
                instantiateItem(view, realPosition);
            } else {
                view = instantiateItem(null, realPosition);
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(width, FrameLayout.LayoutParams.WRAP_CONTENT);
                params.leftMargin = width * i;
                layout.addView(view, params);
                view.setOnClickListener(this);
                view.setOnLongClickListener(this);
            }
            view.setTag(R.id.spec_tag, realPosition);
            realPosition++;
        }
        return v;
    }

    private View getSingleItemView() {

        return null;
    }

    private View instantiateItem(View v, final int position) {
        ItemHolder h;
        if (v == null) {
            h = new ItemHolder();
            v = mInflater.inflate(R.layout.album_item_grid_view, null);
            h.Image = (ImageView) v.findViewById(R.id.image);
            h.Text = (TextView) v.findViewById(R.id.text);
            h.Author = (TextView) v.findViewById(R.id.additional);
            h.CoverErrorButton = (ImageButton) v.findViewById(R.id.error_no_cover);
            h.Image.setLayoutParams(new RelativeLayout.LayoutParams(imageWidth, imageHeight));
            h.Text.setTypeface(Typefacer.Light);
            h.Author.setTypeface(Typefacer.Light);
            v.setTag(h);
        } else {
            h = (ItemHolder) v.getTag();
        }
        Album album = getItem(position);
        String key = album.author + album.Title + "_" + imageWidth + "_" + imageHeight;
        MIM2.ImageLoadObject loadObject = new MIM2.ImageLoadObject(key, key)
                .setListener(new AlbumCoverLoadListener(h))
                .setWidth(width).setHeight(height)
                .setObj(album).setUseDisplayer(true);
        mim.loadImage(loadObject, h.Image);
        h.Text.setText(album.Title);
        h.Author.setText(album.author);
        h.CoverErrorButton.setVisibility(View.GONE);
        if (mContext instanceof FragmentActivity)
            h.CoverErrorButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlbumCoverFetchDialog dialog = AlbumCoverFetchDialog.makeInstance(getItem(position));
                    dialog.show(((FragmentActivity) mContext).getSupportFragmentManager(), AlbumCoverFetchDialog.TAG);
                }
            });
        return v;
    }

    class ItemHolder {
        ImageView Image;
        TextView Text;
        TextView Author;
        ImageButton CoverErrorButton;
    }

    @Override
    public void onClick(View v) {
        if (mItemClickListener != null) {
            mItemClickListener.onItemClick(v, (Integer) v.getTag(R.id.spec_tag));
        }
    }

    @Override
    public boolean onLongClick(View v) {
        return mItemClickListener != null
                ? mItemClickListener.onItemLongClick(v, (Integer) v.getTag(R.id.spec_tag)) : false;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (scrollState == SCROLL_STATE_FLING) {
            mim.pause();
        } else {
            mim.resume();
        }
    }

    private final class AlbumCoverLoadListener implements MIM2.OnImageLoadEventListener {

        private final ItemHolder holder;

        public AlbumCoverLoadListener(ItemHolder holder) {
            this.holder = holder;
        }

        @Override
        public void onImageLoadEvent(MIM2.OnImageLoadEventListener.IMAGE_LOAD_EVENT event, Object obj) {
            if (event == MIM2.OnImageLoadEventListener.IMAGE_LOAD_EVENT.END) {
                if (obj instanceof MIM2.ImageLoadObject) {
                    if (((MIM2.ImageLoadObject) obj).getResultBitmapObject() == null) {
                        holder.CoverErrorButton.setVisibility(View.VISIBLE);
                        mFlipDisplayer.display(holder.CoverErrorButton);
                    }
                }
            }
        }
    }

}
