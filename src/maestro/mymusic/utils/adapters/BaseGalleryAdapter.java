package maestro.mymusic.utils.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import maestro.mymusic.R;

/**
 * Created by artyom on 7/24/14.
 */
public abstract class BaseGalleryAdapter<T> extends BaseUpdateAdapter<T> {

    public BaseGalleryAdapter(Context context) {
        super(context);
    }

    @Override
    public View getView(int position, View v, ViewGroup viewGroup) {
        BaseGalleryHolder holder;
        if (v == null) {
            holder = new BaseGalleryHolder();
            v = mInflater.inflate(R.layout.gallery_item_view, null);
            holder.Image = (ImageView) v.findViewById(R.id.image);
            holder.Title = (TextView) v.findViewById(R.id.title);
            holder.Additional = (TextView) v.findViewById(R.id.additional);
            v.setTag(holder);
        } else {
            holder = (BaseGalleryHolder) v.getTag();
        }
        instantiateItem(getItem(position), holder);
        return v;
    }

    public abstract void instantiateItem(T item, BaseGalleryHolder holder);

    public class BaseGalleryHolder {
        ImageView Image;
        TextView Title;
        TextView Additional;
    }

}