package maestro.mymusic.utils.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;
import com.dream.android.mim.MIM2;
import com.dream.android.mim.MIMAlphaDisplayer;
import com.dream.android.mim.MIMManager;
import com.msoft.android.mplayer.lib.models.Genre;
import maestro.mymusic.Constants;
import maestro.mymusic.R;
import maestro.mymusic.utils.ComplexCoverMaker;
import maestro.mymusic.utils.Typefacer;

/**
 * Created by artyom on 7/25/14.
 */
public class SimpleGenreAdapter extends BaseUpdateAdapter<Genre> {

    private MIM2 mim;
    private int width, height;

    public SimpleGenreAdapter(Context context, int width) {
        super(context);
        mim = MIMManager.getInstance().getMIM(Constants.COMPLEX_MIM);
        if (mim == null) {
            mim = new MIM2(context.getApplicationContext())
                    .setDisplayAnimationEnable(true)
                    .setDisplayFromCacheEnable(true)
                    .setDisplayer(new MIMAlphaDisplayer(300))
                    .prepareDiskCache()
                    .setMaker(new ComplexCoverMaker());
            MIMManager.getInstance().addMIM(Constants.COMPLEX_MIM, mim);
        }
        this.width = width;
        height = context.getResources().getDimensionPixelSize(R.dimen.additional_list_item_height);
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).Id;
    }

    @Override
    public View getView(int position, View v, ViewGroup viewGroup) {
        Holder holder;
        if (v == null) {
            holder = new Holder();
            v = mInflater.inflate(R.layout.additional_list_item_view, null);
            holder.Image = (ImageView) v.findViewById(R.id.image);
            holder.Title = (TextView) v.findViewById(R.id.title);
            holder.Additional = (TextView) v.findViewById(R.id.additional);
            holder.Title.setTypeface(Typefacer.Light);
            holder.Additional.setTypeface(Typefacer.LightItalic);
            v.setTag(holder);
        } else {
            holder = (Holder) v.getTag();
        }
        Genre item = getItem(position);
        String key = item.Title;
        MIM2.ImageLoadObject loadObject = new MIM2.ImageLoadObject(key, key)
                .setObj(item).setWidth(width).setHeight(height);
        mim.loadImage(loadObject, holder.Image);
        holder.Title.setText(item.Title);
        holder.Additional.setText(item.getCount() + " " + mContext.getResources().getString(R.string.tracks));
        return v;
    }

    class Holder {
        ImageView Image;
        TextView Title;
        TextView Additional;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (scrollState == SCROLL_STATE_FLING) {
            mim.pause();
        } else {
            mim.resume();
        }
    }
}
