package maestro.mymusic.utils.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.TextView;
import com.dream.android.mim.MIM2;
import com.dream.android.mim.MIMManager;
import com.msoft.android.helpers.MFormatter;
import com.msoft.android.mplayer.lib.models.*;
import com.msoft.android.service.MusicPlayerEngine;
import maestro.mymusic.Constants;
import maestro.mymusic.R;
import maestro.mymusic.utils.AlbumCoverMaker;
import maestro.mymusic.utils.MFlipDisplayer;
import maestro.mymusic.utils.Typefacer;

/**
 * Created by artyom on 7/11/14.
 */
public class UniqRowAdapter extends BaseUpdateAdapter<BaseMediaItem> implements MusicPlayerEngine.MusicPlayerListener {

    public static final String TAG = UniqRowAdapter.class.getSimpleName();

    private MIM2 mim = MIMManager.getInstance().getMIM(Constants.SONG_COVER_MIM);
    private MFlipDisplayer mFlipDisplayer = new MFlipDisplayer();
    private MusicPlayerEngine mEngine = MusicPlayerEngine.getInstance();
    private int additionalSidePadding;
    private int coverSize;
    private boolean isImagesEnable = true;
    private boolean isForPlayView = false;
    private float textSizeBig, textSizeMid;

    public UniqRowAdapter(Context context) {
        this(context, 0);
    }

    public UniqRowAdapter(Context context, int additionalSidePadding) {
        super(context);
        this.additionalSidePadding = additionalSidePadding;
        coverSize = mContext.getResources().getDimensionPixelSize(R.dimen.base_cover_size);
        if (mim == null) {
            mim = new MIM2(context.getApplicationContext()).setDefaultSize(coverSize, coverSize)
                    .setMaker(new AlbumCoverMaker(context.getApplicationContext()));
            MIMManager.getInstance().addMIM(Constants.SONG_COVER_MIM, mim);
        }
        textSizeBig = context.getResources().getDimension(R.dimen.txt_header);
        textSizeMid = context.getResources().getDimension(R.dimen.txt_big);

    }

    public UniqRowAdapter(Context context, boolean enableImages, int additionalSidePadding) {
        this(context, additionalSidePadding);
        isImagesEnable = enableImages;
    }

    public UniqRowAdapter setIsForPlayView(boolean isForPlayView) {
        this.isForPlayView = isForPlayView;
        return this;
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).Id;
    }

    @Override
    public View getView(int position, View v, ViewGroup viewGroup) {
        Holder h;
        if (v == null) {
            h = new Holder();
            v = mInflater.inflate(R.layout.song_item_view, null);
            h.Image = (ImageView) v.findViewById(R.id.image);
            h.PlayIndicator = (ImageView) v.findViewById(R.id.play_indicator);
            h.NoCover = (ImageView) v.findViewById(R.id.no_cover_image_view);
            h.Title = (TextView) v.findViewById(R.id.title);
            h.Author = (TextView) v.findViewById(R.id.author);
            h.Time = (TextView) v.findViewById(R.id.time);
            h.Title.setTypeface(Typefacer.Light);
            h.Author.setTypeface(Typefacer.Light);
            h.Time.setTypeface(Typefacer.Light);
            if (additionalSidePadding != 0) {
                v.setPadding(additionalSidePadding, v.getPaddingTop(), additionalSidePadding, v.getPaddingBottom());
            }
            if (isForPlayView) {
                h.Title.setTextColor(Color.WHITE);
                h.Author.setTextColor(Color.WHITE);
                h.Time.setTextColor(Color.WHITE);
            }
            v.setTag(h);
        } else {
            h = (Holder) v.getTag();
        }
        instantiateItem(getItem(position), h);
        return v;
    }

    final void instantiateItem(BaseMediaItem mediaItem, Holder h) {
        if (mediaItem instanceof HeaderItem) {
            h.Title.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSizeBig);
            h.NoCover.setVisibility(View.GONE);
            h.Image.setVisibility(View.GONE);
            h.Time.setVisibility(View.GONE);
            h.PlayIndicator.setVisibility(View.GONE);
            h.Author.setVisibility(View.GONE);
            h.Title.setText(mediaItem.Title);
            return;
        }

        h.Title.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSizeMid);
        h.Image.setVisibility(View.VISIBLE);
        h.Time.setVisibility(View.VISIBLE);
        h.PlayIndicator.setVisibility(View.VISIBLE);
        h.Author.setVisibility(View.VISIBLE);
        h.NoCover.setVisibility(View.VISIBLE);

        if (mediaItem instanceof Song) {
            Song item = (Song) mediaItem;
            h.Title.setText(item.Title);
            h.Author.setText(item.Author);
            h.Time.setText(MFormatter.formatTimeFromMillis(item.Duration));
            String key = item.Title + "_song_" + item.Duration;
            MIM2.ImageLoadObject loadObject = new MIM2.ImageLoadObject(key, key).setObj(item)
                    .setWidth(coverSize).setHeight(coverSize).setUseDisplayer(true);
            mim.loadImage(loadObject, h.Image);
            if (mEngine.isCurrentPlayingSong(item)) {
                h.NoCover.setVisibility(View.GONE);
                h.PlayIndicator.setVisibility(View.VISIBLE);
                mFlipDisplayer.display(h.PlayIndicator);
            } else {
                h.NoCover.setVisibility(View.VISIBLE);
                h.PlayIndicator.setVisibility(View.GONE);
            }
        } else if (mediaItem instanceof Album) {
            Album album = (Album) mediaItem;
            h.Title.setText(album.Title);
            h.Author.setText(album.author);
            h.Time.setVisibility(View.GONE);
            String key = album.author + album.Title + "_" + coverSize + "_" + coverSize;
            MIM2.ImageLoadObject loadObject = new MIM2.ImageLoadObject(key, key)
                    .setWidth(coverSize).setHeight(coverSize)
                    .setObj(album).setUseDisplayer(true);
            mim.loadImage(loadObject, h.Image);
            h.PlayIndicator.setVisibility(View.GONE);
        } else if (mediaItem instanceof Artist) {
            Artist artist = (Artist) mediaItem;
            h.Title.setText(artist.Title);
            h.Author.setText(artist.Count + " " + mContext.getResources().getString(R.string.tracks));
            h.Time.setVisibility(View.GONE);
            String key = artist.Title + "_artist";
            MIM2.ImageLoadObject loadObject = new MIM2.ImageLoadObject(key, key).setObj(artist)
                    .setWidth(coverSize).setHeight(coverSize);
            mim.loadImage(loadObject, h.Image);
            h.PlayIndicator.setVisibility(View.GONE);
        } else if (mediaItem instanceof Genre) {
            Genre genre = (Genre) mediaItem;
            h.Title.setText(genre.Title);
            h.Author.setText(String.valueOf(genre.getCount()) + " " + mContext.getResources().getString(R.string.tracks));
            h.Time.setVisibility(View.GONE);
            MIM2.ImageLoadObject loadObject = new MIM2.ImageLoadObject(genre.Title, genre.Title)
                    .setObj(genre).setWidth(coverSize).setHeight(coverSize);
            mim.loadImage(loadObject, h.Image);
            h.PlayIndicator.setVisibility(View.GONE);
        }

        if (!isImagesEnable) {
            h.Image.setVisibility(View.GONE);
            h.PlayIndicator.setVisibility(View.GONE);
            h.NoCover.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean isEnabled(int position) {
        return !(getItem(position) instanceof HeaderItem);
    }

    @Override
    public void onMusicPlayerEvent(MusicPlayerEngine.MUSIC_PLAYER_EVENT event) {
        switch (event) {
            case TIME_UPDATE:
                break;
            default:
                notifyDataSetInvalidated();
        }
    }

    class Holder {
        ImageView NoCover;
        ImageView PlayIndicator;
        ImageView Image;
        TextView Title;
        TextView Author;
        TextView Time;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (scrollState == SCROLL_STATE_FLING) {
            mim.pause();
        } else {
            mim.resume();
        }
    }

    public static class HeaderItem extends BaseMediaItem {

        public HeaderItem(String title) {
            Title = title;
        }

    }

}
