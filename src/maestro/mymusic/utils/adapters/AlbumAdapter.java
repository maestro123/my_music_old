package maestro.mymusic.utils.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.dream.android.mim.MIM2;
import com.dream.android.mim.MIMAbstractPostMaker;
import com.dream.android.mim.MIMManager;
import com.msoft.android.mplayer.lib.models.Album;
import maestro.mymusic.Constants;
import maestro.mymusic.R;
import maestro.mymusic.ui.AlbumCoverFetchDialog;
import maestro.mymusic.utils.*;

import java.util.HashMap;

/**
 * Created by artyom on 7/10/14.
 */
public class AlbumAdapter extends MGridAdapter<Album> implements View.OnTouchListener {

    private MIM2 mim;
    private MFlipDisplayer mFlipDisplayer = new MFlipDisplayer();
    private MSlideDownDisplayer mSlideDispalyer = new MSlideDownDisplayer();
//    private HashMap<String, int[]> mColorsCache;

    public AlbumAdapter(Context context, int totalWidth, int topPadding) {
        super(context, totalWidth, topPadding);
        mim = MIMManager.getInstance().getMIM(Constants.ALBUM_COVER_MIM);
        if (mim == null) {
            mim = new MIM2(context.getApplicationContext())
                    .prepareDiskCache()
                    .setMaker(new AlbumCoverMaker(context.getApplicationContext()));
            MIMManager.getInstance().addMIM(Constants.ALBUM_COVER_MIM, mim);
        }
//        mColorsCache = (HashMap<String, int[]>) GlobalCache.getInstance().getCache(Constants.ALBUM_COLOR_CACHE);
//        if (mColorsCache == null) {
//            mColorsCache = new HashMap<String, int[]>();
//            GlobalCache.getInstance().addCache(Constants.ALBUM_COLOR_CACHE, mColorsCache);
//        }
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View instantiateItem(View v, final int position, int width, int height) {
        Holder h;
        if (v == null) {
            h = new Holder();
            v = mInflater.inflate(R.layout.album_item_view, null);
            h.Image = (ImageView) v.findViewById(R.id.image);
            h.Text = (TextView) v.findViewById(R.id.text);
            h.CoverError = (ImageButton) v.findViewById(R.id.error_no_cover);
            h.Text.setTypeface(Typefacer.Light);
            h.Parent = v;
            v.setTag(h);
        } else {
            h = (Holder) v.getTag();
        }
        Album album = getItem(position);
        String key = album.author + album.Title + "_" + width + "_" + height;
        MIM2.ImageLoadObject loadObject = new MIM2.ImageLoadObject(key, key)
//                .setListener(new AlbumCoverLoadListener(h))
                .setWidth(width).setHeight(height)
                .setObj(album).setUseDisplayer(true);
        if (album.artPath != null) {
            h.CoverError.setVisibility(View.GONE);
            h.Text.setVisibility(View.GONE);
        } else {
            h.CoverError.setVisibility(View.VISIBLE);
            h.Text.setVisibility(View.VISIBLE);
        }

//        if (mColorsCache.containsKey(key) && mColorsCache.get(key) != null) {
//            v.setBackgroundColor(mColorsCache.get(key)[0]);
//        } else {
//            v.setBackgroundColor(Color.TRANSPARENT);
//            loadObject.setPostMaker(new AlbumPostMaker());
//        }
        mim.loadImage(loadObject, h.Image);
        h.Text.setText(album.Title);
        if (mContext instanceof FragmentActivity)
            h.CoverError.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlbumCoverFetchDialog dialog = AlbumCoverFetchDialog.makeInstance(getItem(position));
                    dialog.show(((FragmentActivity) mContext).getSupportFragmentManager(), AlbumCoverFetchDialog.TAG);
                }
            });

        return v;
    }

    class Holder {
        View Parent;
        ImageButton CoverError;
        ImageView Image;
        TextView Text;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        if (scrollState == SCROLL_STATE_FLING) {
            mim.pause();
        } else {
            mim.resume();
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction() & event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                v.animate().scaleX(1.3f).scaleY(1.3f).start();
                return true;
            case MotionEvent.ACTION_MOVE:

                return true;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                v.animate().scaleX(1f).scaleY(1f).start();
                onClick(v);
                return true;
        }
        return false;
    }

    private final class AlbumCoverLoadListener implements MIM2.OnImageLoadEventListener {

        private final Holder holder;

        public AlbumCoverLoadListener(Holder holder) {
            this.holder = holder;
        }

        @Override
        public void onImageLoadEvent(MIM2.OnImageLoadEventListener.IMAGE_LOAD_EVENT event, Object obj) {
            if (event == MIM2.OnImageLoadEventListener.IMAGE_LOAD_EVENT.END) {
                if (obj instanceof MIM2.ImageLoadObject) {
                    MIM2.ImageLoadObject loadObject = (MIM2.ImageLoadObject) obj;
                    if (loadObject.getResultBitmapObject() == null) {
                        holder.CoverError.setVisibility(View.VISIBLE);
                        mFlipDisplayer.display(holder.CoverError);
                        holder.Text.setVisibility(View.VISIBLE);
                    } else {
//                        if (mColorsCache.containsKey(loadObject.getKey())
//                                && mColorsCache.get(loadObject.getKey()) != null)
//                            holder.Parent.setBackgroundColor(mColorsCache.get(loadObject.getKey())[0]);
                    }
                }
            }
        }
    }

    private final class AlbumPostMaker extends MIMAbstractPostMaker {

        @Override
        public Bitmap processBitmap(MIM2.ImageLoadObject object, Bitmap bitmap) {
            if (bitmap != null) {
//                int[] colors = ColorAnalyzer.getMostColors(bitmap, 1, 2);
//                mColorsCache.put(object.getKey(), colors);
//                Log.e(TAG, "mColorsCache size = " + mColorsCache.size() + ", " + colors);
            }
            return bitmap;
        }

    }

}
