package maestro.mymusic.utils.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.dream.android.mim.MIM2;
import com.dream.android.mim.MIMManager;
import com.msoft.android.mplayer.lib.models.Artist;
import maestro.mymusic.Constants;
import maestro.mymusic.R;
import maestro.mymusic.utils.AlbumCoverMaker;
import maestro.mymusic.utils.Typefacer;

/**
 * Created by artyom on 7/11/14.
 */
public class ArtistsAdapter extends BaseUpdateAdapter<Artist> {

    private MIM2 mim;
    private int coverSize;

    public ArtistsAdapter(Context context) {
        super(context);
        coverSize = mContext.getResources().getDimensionPixelSize(R.dimen.base_cover_size);
        mim = MIMManager.getInstance().getMIM(Constants.SONG_COVER_MIM);
        if (mim == null) {
            mim = new MIM2(context.getApplicationContext()).setDefaultSize(coverSize, coverSize)
                    .setMaker(new AlbumCoverMaker(context.getApplicationContext()));
            MIMManager.getInstance().addMIM(Constants.SONG_COVER_MIM, mim);
        }
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).Id;
    }

    @Override
    public View getView(int position, View v, ViewGroup viewGroup) {
        Holder h;
        if (v == null) {
            h = new Holder();
            v = mInflater.inflate(R.layout.song_item_view, null);
            h.Image = (ImageView) v.findViewById(R.id.image);
            h.Title = (TextView) v.findViewById(R.id.title);
            h.Author = (TextView) v.findViewById(R.id.author);
            h.Time = (TextView) v.findViewById(R.id.time);
            h.Title.setTypeface(Typefacer.Light);
            h.Author.setTypeface(Typefacer.Light);
            h.Time.setTypeface(Typefacer.Light);
            v.setTag(h);
        } else {
            h = (Holder) v.getTag();
        }
        Artist item = getItem(position);
        h.Title.setText(item.Title);
        h.Author.setVisibility(View.GONE);
        h.Time.setText(String.valueOf(item.Count));
        String key = item.Title + "_artist";
        MIM2.ImageLoadObject loadObject = new MIM2.ImageLoadObject(key, key).setObj(item)
                .setWidth(coverSize).setHeight(coverSize);
        mim.loadImage(loadObject, h.Image);
        return v;
    }

    class Holder {
        ImageView Image;
        TextView Title;
        TextView Author;
        TextView Time;
    }
}
