package maestro.mymusic.utils.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import maestro.mymusic.R;
import maestro.mymusic.utils.TwoWayView;

/**
 * Created by artyom on 7/24/14.
 */
public abstract class MGalleryAdapter<T> extends BaseUpdateAdapter<T> {

    public MGalleryAdapter(Context context) {
        super(context);
    }

    @Override
    public View getView(int position, View v, ViewGroup viewGroup) {
        GalleryHolder h;
        if (v == null) {
            h = new GalleryHolder();
            v = mInflater.inflate(R.layout.m_gallery_adapter_item_view, null);
            h.Title = (TextView) v.findViewById(R.id.title);
            h.List = (TwoWayView) v.findViewById(R.id.list);
            h.List.setOrientation(TwoWayView.Orientation.HORIZONTAL);
            h.Progress = (ProgressBar) v.findViewById(R.id.progress);
            v.setTag(h);
        } else {
            h = (GalleryHolder) v.getTag();
        }
        T item = getItem(position);
        instantiateItem(item, h);
        return v;
    }

    public abstract void instantiateItem(T item, GalleryHolder holder);

    public class GalleryHolder {
        TextView Title;
        TwoWayView List;
        ProgressBar Progress;
    }

}
