package maestro.mymusic.utils.adapters;

import android.content.Context;
import android.view.View;
import com.dream.android.mim.MIM2;
import com.dream.android.mim.MIMManager;
import com.msoft.android.mplayer.lib.MediaStoreHelper;
import com.msoft.android.mplayer.lib.models.Genre;
import com.msoft.android.mplayer.lib.models.Song;
import maestro.mymusic.Constants;
import maestro.mymusic.R;
import maestro.mymusic.utils.AlbumCoverMaker;
import maestro.mymusic.utils.GalleryItemsLoader;

import java.util.List;

/**
 * Created by artyom on 7/11/14.
 */
public class GenresAdapter extends MGalleryAdapter<Genre> implements GalleryItemsLoader.GalleryItemsLoaderMaker {

    private MIM2 mim = MIMManager.getInstance().getMIM(Constants.SONG_COVER_MIM);
    private int coverSize;

    public GenresAdapter(Context context) {
        super(context);
        coverSize = mContext.getResources().getDimensionPixelSize(R.dimen.base_cover_size);
        if (mim == null) {
            mim = new MIM2(context.getApplicationContext()).setDefaultSize(coverSize, coverSize)
                    .setMaker(new AlbumCoverMaker(context.getApplicationContext()));
            MIMManager.getInstance().addMIM(Constants.SONG_COVER_MIM, mim);
        }
    }

    @Override
    public void instantiateItem(Genre item, final GalleryHolder holder) {
        holder.Title.setText(item.Title);
        holder.Progress.setVisibility(View.VISIBLE);
        holder.List.setAdapter(null);
        if (holder.List.getAdapter() == null) {
            holder.List.setAdapter(new GenreGalleryAdapter(mContext));
        } else {
            ((BaseUpdateAdapter) holder.List.getAdapter()).update(null);
        }
        GalleryItemsLoader.getInstance().load(item, holder.List, new GalleryItemsLoader.OnGalleryItemsLoaderEvent() {
            @Override
            public void onGalleryItemsLoaderEvent(Object result) {
                holder.Progress.setVisibility(View.GONE);
                ((BaseGalleryAdapter) holder.List.getAdapter()).update((Song[]) result);
            }
        }, this, false);
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).Id;
    }

    @Override
    public Object load(Object object) {
        List<Song> songs = MediaStoreHelper.getSongsByGenre(mContext, ((Genre) object).Id);
        return songs != null ? songs.toArray(new Song[songs.size()]) : null;
    }

    private class GenreGalleryAdapter extends BaseGalleryAdapter<Song> {

        private MIM2 mim;
        private int size;

        public GenreGalleryAdapter(Context context) {
            super(context);
            mim = MIMManager.getInstance().getMIM(Constants.SONG_COVER_MIM);
            if (mim == null) {
                mim = new MIM2(context.getApplicationContext()).setMaker(new AlbumCoverMaker(context.getApplicationContext()));
                MIMManager.getInstance().addMIM(Constants.SONG_COVER_MIM, mim);
            }
            size = mContext.getResources().getDimensionPixelSize(R.dimen.gallery_item_size);
        }

        @Override
        public void instantiateItem(Song item, BaseGalleryHolder holder) {
            String key = item.Author + item.Title + "_" + size + "_" + size;
            MIM2.ImageLoadObject loadObject = new MIM2.ImageLoadObject(key, key)
                    .setWidth(size).setHeight(size).setObj(item);
            mim.loadImage(loadObject, holder.Image);
            holder.Title.setText(item.Title);
            holder.Additional.setText(item.Author);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }
    }

//    @Override
//    public View getView(int position, View v, ViewGroup viewGroup) {
//        Holder h;
//        if (v == null) {
//            h = new Holder();
//            v = mInflater.inflate(R.layout.song_item_view, null);
//            h.Image = (ImageView) v.findViewById(R.id.image);
//            h.Title = (TextView) v.findViewById(R.id.title);
//            h.Author = (TextView) v.findViewById(R.id.author);
//            h.Time = (TextView) v.findViewById(R.id.time);
//            h.Title.setTypeface(Typefacer.Light);
//            h.Author.setTypeface(Typefacer.Light);
//            h.Time.setTypeface(Typefacer.Light);
//            v.setTag(h);
//        } else {
//            h = (Holder) v.getTag();
//        }
//        Genre item = getItem(position);
//        h.Title.setText(item.Title);
////        h.Author.setText(item.Author);
//        h.Author.setVisibility(View.GONE);
////        h.Time.setText(MFormatter.formatTimeFromMillis(item.Duration));
//        String key = item.Title + "_genre";
//        MIM2.ImageLoadObject loadObject = new MIM2.ImageLoadObject(key, key).setObj(item);
//        mim.loadImage(loadObject, h.Image);
//        return v;
//    }

//    class Holder {
//        ImageView Image;
//        TextView Title;
//        TextView Author;
//        TextView Time;
//    }
}
