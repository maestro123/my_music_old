package maestro.mymusic.utils;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.provider.MediaStore;
import com.dream.android.mim.MIM2;
import com.dream.android.mim.MIMDefaultMaker;
import com.dream.android.mim.MIMInternetMaker;
import com.msoft.android.mplayer.lib.MediaStoreHelper;
import com.msoft.android.mplayer.lib.models.Album;
import com.msoft.android.mplayer.lib.models.Artist;
import com.msoft.android.mplayer.lib.models.Song;
import maestro.lastfm.lib.*;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by artyom on 7/10/14.
 */
public class AlbumCoverMaker extends MIMDefaultMaker {

    public static final String TAG = AlbumCoverMaker.class.getSimpleName();

    private Context mContext;
    private MIMInternetMaker mimInternetMaker = new MIMInternetMaker(false);

    public AlbumCoverMaker(Context context) {
        mContext = context;
    }

    @Override
    public Bitmap getBitmap(MIM2.ImageLoadObject loadObject, Context ctx) {
        ctx = ctx != null ? ctx : mContext;
        try {
            String artPath = null;
            byte[] bytes = null;
            if (loadObject.getObj() instanceof Album) {
                artPath = ((Album) loadObject.getObj()).artPath;
            } else if (loadObject.getObj() instanceof Song) {
                Cursor cur = MediaStoreHelper
                        .getAlbumCoversCursor(ctx, ((Song) loadObject.getObj()).ParentId);
                if (cur.getCount() == 0)
                    return null;
                cur.moveToFirst();
                artPath = cur.getCount() > 0 ? cur.getString(cur
                        .getColumnIndexOrThrow(MediaStore.Audio.Albums.ALBUM_ART))
                        : null;
                cur.close();
            } else if (loadObject.getObj() instanceof Artist) {
                Artist artist = (Artist) loadObject.getObj();
                String url = LastFmApi.buildArtistInfoRequest(artist.Title);
                Object response = LastFmApi.getArtistInfo(url, null, false);

                if (response instanceof String || response instanceof LastFmArtist) {
                    try {
                        LastFmArtist lastFmArtist = response instanceof String ? new LastFmArtist(new JSONObject((String) response))
                                : (LastFmArtist) response;
                        LastFmImage image = lastFmArtist.getImage(LastFmApi.IMAGE_EXTRA_LARGE);
                        if (image != null) {
                            return loadObject.postProcess(mimInternetMaker.getBitmap(image.getUrl(),
                                    loadObject.getWidth(), loadObject.getHeight()));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (artPath == null) {
                if (loadObject.getObj() instanceof Album) {
                    List<Song> allSongs = MediaStoreHelper.getSongs(ctx, ((Album) loadObject.getObj()).Id);
                    if (allSongs != null && allSongs.size() > 0) {
                        for (Song _s : allSongs) {
                            bytes = tryGetArt(_s.Path);
                            if (bytes != null)
                                break;
                        }
                    }
                } else if (loadObject.getObj() instanceof Song) {
                    bytes = tryGetArt(((Song) loadObject.getObj()).Path);
                }
            }

            if (artPath == null && bytes == null) {
                try {
                    LastFmImage image = null;
                    if (loadObject.getObj() instanceof Album) {
                        Album album = (Album) loadObject.getObj();
                        Object result = LastFmApi.getAlbumInfo(LastFmApi.buildAlbumInfoRequest(album.Title, album.author), null, false);
                        if (result instanceof String || result instanceof LastFmAlbum) {
                            LastFmAlbum fmAlbum = result instanceof String ? new LastFmAlbum(new JSONObject((String) result))
                                    : (LastFmAlbum) result;
                            image = fmAlbum.getBiggestImage();
                        }
                    } else if (loadObject.getObj() instanceof Song) {
                        Song song = (Song) loadObject.getObj();
                        Object result = LastFmApi.getTrackInfo(LastFmApi.buildTrackInfoRequest(song.Title, song.Author), null, false);
                        if (result instanceof String || result instanceof LastFmTrack) {
                            LastFmTrack fmTrack = result instanceof String ? new LastFmTrack(new JSONObject((String) result))
                                    : (LastFmTrack) result;
                            LastFmAlbum fmAlbum = fmTrack.getAlbum();
                            if (fmAlbum != null)
                                image = fmAlbum.getBiggestImage();
                        }
                    }
                    if (image != null)
                        return loadObject.postProcess(new MIMInternetMaker(false).getBitmap(image.getUrl(), loadObject.getWidth(), loadObject.getHeight()));

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            BitmapFactory.Options opt = new BitmapFactory.Options();
            opt.inJustDecodeBounds = true;
            if (artPath != null)
                BitmapFactory.decodeFile(artPath, opt);
            else if (bytes != null) BitmapFactory.decodeByteArray(bytes, 0, bytes.length, opt);
            opt.inJustDecodeBounds = false;
            opt.inSampleSize = calculateInSampleSize(opt, loadObject.getWidth(), loadObject.getHeight());
            Bitmap out = artPath != null ? BitmapFactory.decodeFile(artPath, opt)
                    : bytes != null ? BitmapFactory.decodeByteArray(bytes, 0, bytes.length, opt) : null;
            return loadObject.postProcess(out);
        } catch (
                Exception e
                )

        {
            e.printStackTrace();
        }

        return null;
    }

    private byte[] tryGetArt(String path) {
        MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
        metaRetriever.setDataSource(path);
        return metaRetriever.getEmbeddedPicture();

    }
}
