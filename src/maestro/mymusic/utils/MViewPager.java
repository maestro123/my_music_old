package maestro.mymusic.utils;

import android.content.Context;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewConfigurationCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;

/**
 * Created by artyom on 8/29/14.
 */
public class MViewPager extends ViewPager {

    public static final String TAG = MViewPager.class.getSimpleName();

    public MViewPager(Context context) {
        super(context);
        init();
    }

    public MViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private ExternalTouchListener externalTouchListener;

    public interface ExternalTouchListener {
        public boolean onTouch(View view, MotionEvent event);
    }

    public void setExternalTouchListener(ExternalTouchListener listener) {
        externalTouchListener = listener;
    }

    private float mLastMotionX, mLastMotionY, mInitialMotionX, mInitialMotionY;
    private int mActivePointerId, mTouchSlop;
    private boolean mIsBeingDragged, holdByAnother, externalEnable;

    private void init() {
        final ViewConfiguration configuration = ViewConfiguration.get(getContext());
        mTouchSlop = ViewConfigurationCompat.getScaledPagingTouchSlop(configuration);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (!externalEnable)
            super.onTouchEvent(ev);
        final int pointerIndex = MotionEventCompat.findPointerIndex(ev, mActivePointerId);
        final float x = MotionEventCompat.getX(ev, pointerIndex);
        final float xDiff = Math.abs(x - mLastMotionX);
        final float y = MotionEventCompat.getY(ev, pointerIndex);
        final float yDiff = Math.abs(y - mLastMotionY);
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mLastMotionX = mInitialMotionX = ev.getX();
                mLastMotionY = mInitialMotionY = ev.getY();
                mActivePointerId = MotionEventCompat.getPointerId(ev, 0);
                break;
            case MotionEvent.ACTION_MOVE:
                if (!mIsBeingDragged) {
                    if (xDiff > mTouchSlop && xDiff > yDiff) {
                        mIsBeingDragged = true;
                        mLastMotionX = x - mInitialMotionX > 0 ? mInitialMotionX + mTouchSlop :
                                mInitialMotionX - mTouchSlop;
                        mLastMotionY = y;
                    } else if (!externalEnable) {
                        externalEnable = yDiff > mTouchSlop * 5;
                    }
                    if (externalEnable && externalTouchListener != null) {
                        holdByAnother = externalTouchListener.onTouch(this, ev);
                    }
                }
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                if (externalEnable && externalTouchListener != null) {
                    holdByAnother = externalTouchListener.onTouch(this, ev);
                }
                mIsBeingDragged = false;
                holdByAnother = false;
                externalEnable = false;
                break;
        }
        return true;
    }

}
