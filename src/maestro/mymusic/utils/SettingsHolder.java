package maestro.mymusic.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.PreferenceManager;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashMap;

/**
 * Created by artyom on 8/29/14.
 */
public class SettingsHolder {

    private static volatile SettingsHolder instance;

    public static synchronized final SettingsHolder getInstance() {
        SettingsHolder localInstance = instance;
        if (localInstance == null) {
            synchronized (SettingsHolder.class) {
                localInstance = instance;
                if (localInstance == null) {
                    localInstance = instance = new SettingsHolder();
                }
            }
        }
        return localInstance;
    }

    SettingsHolder() {
    }

    public static final String PREF_ALBUM_VIEW_TYPE = "pref_album_view_type";
    public static final String PREF_PRIMARY_COLOR = "pref_primary_color";
    public static final String PREF_BACKGROUND_COLOR = "pref_background_color";

    public static int COLORS[] = {Color.parseColor("#e51c23"), Color.parseColor("#e91e63"), Color.parseColor("#9c27b0"),
            Color.parseColor("#673ab7"), Color.parseColor("#3f51b5"), Color.parseColor("#5677fc"),
            Color.parseColor("#03a9f4"), Color.parseColor("#00bcd4"), Color.parseColor("#009688"),
            Color.parseColor("#259b24"), Color.parseColor("#8bc34a"), Color.parseColor("#cddc39"),
            Color.parseColor("#ffeb3b"), Color.parseColor("#ffc107"), Color.parseColor("#ff9800"),
            Color.parseColor("#ff5722"), Color.parseColor("#795548"), Color.parseColor("#9e9e9e"),
            Color.parseColor("#607d8b")};

    public static int[] BACKGROUND_COLOR = {Color.WHITE, Color.BLACK};

    private Context mContext;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;

    private HashMap<String, OnUIPreferenceChangeListener> mUIListeners
            = new HashMap<String, OnUIPreferenceChangeListener>();

    public interface OnUIPreferenceChangeListener {
        public void onUIPreferenceChange(String key, Object value);
    }

    public void attachOnUIPreferenceChangeListener(String key, OnUIPreferenceChangeListener listener) {
        synchronized (mUIListeners) {
            if (mUIListeners.containsKey(key))
                mUIListeners.remove(key);
            mUIListeners.put(key, listener);
        }
    }

    public void detachOnUIPreferenceChangeListener(String key) {
        synchronized (mUIListeners) {
            mUIListeners.remove(key);
        }
    }

    private void notifyUIListeners(String key, Object value) {
        synchronized (mUIListeners) {
            Collection<OnUIPreferenceChangeListener> listeners = mUIListeners.values();
            for (OnUIPreferenceChangeListener listener : listeners) {
                listener.onUIPreferenceChange(key, value);
            }
        }
    }

    public boolean isInitialized() {
        return mContext != null;
    }

    public void init(Context context) {
        mContext = context;
        mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        mEditor = mPreferences.edit();
    }

    public void setPrefAlbumViewType(int type) {
        mEditor.putInt(PREF_ALBUM_VIEW_TYPE, type).commit();
    }

    public int getPrefAlbumViewType() {
        return mPreferences.getInt(PREF_ALBUM_VIEW_TYPE, 0);
    }

    public void setPrimaryColor(int color) {
        mEditor.putInt(PREF_PRIMARY_COLOR, color).commit();
        notifyUIListeners(PREF_PRIMARY_COLOR, color);
    }

    public int getPrimaryColor() {
        return mPreferences.getInt(PREF_PRIMARY_COLOR, COLORS[0]);
    }

    public void setBackgroundColor(int color) {
        mEditor.putInt(PREF_BACKGROUND_COLOR, color).commit();
        notifyUIListeners(PREF_BACKGROUND_COLOR, color);
    }

    public int getBackgroundColor() {
        return mPreferences.getInt(PREF_BACKGROUND_COLOR, BACKGROUND_COLOR[0]);
    }

    public int getTextColor() {
        if (getBackgroundColor() == COLORS[0]) {
            return Color.BLACK;
        } else {
            return Color.WHITE;
        }
    }

}
