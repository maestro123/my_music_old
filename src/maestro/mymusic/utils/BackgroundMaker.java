package maestro.mymusic.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import com.dream.android.mim.MIM2;
import com.dream.android.mim.MIMInternetMaker;
import com.dream.android.mim.MIMSearchEngine;

/**
 * Created by artyom on 7/11/14.
 */
public class BackgroundMaker extends MIMInternetMaker {

    public BackgroundMaker(boolean useDefaultHttpClient) {
        super(useDefaultHttpClient);
    }

    @Override
    public Bitmap getBitmap(MIM2.ImageLoadObject loadObject, Context ctx) {
        String path = MIMSearchEngine.search((String) loadObject.getObj());
        Log.e(TAG, "path = " + path);
        if (path != null) {
            return getBitmap(path, loadObject.getWidth(), loadObject.getHeight());
        }
        return null;
    }
}
