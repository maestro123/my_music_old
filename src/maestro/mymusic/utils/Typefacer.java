package maestro.mymusic.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;

/**
 * Created by artyom on 7/11/14.
 */
public class Typefacer {

    public static Typeface Bold;
    public static Typeface BoldItalic;
    public static Typeface Italic;
    public static Typeface Light;
    public static Typeface LightItalic;
    public static Typeface Medium;
    public static Typeface MediumItalic;
    public static Typeface Regular;
    public static Typeface Thin;
    public static Typeface ThinItalic;
    public static Typeface Lobster;

    public Typefacer(Context context) {
        final AssetManager mManager = context.getAssets();
        Bold = Typeface.createFromAsset(mManager, "fonts/Roboto-Bold.ttf");
        BoldItalic = Typeface.createFromAsset(mManager, "fonts/Roboto-BoldItalic.ttf");
        Italic = Typeface.createFromAsset(mManager, "fonts/Roboto-Italic.ttf");
        Light = Typeface.createFromAsset(mManager, "fonts/Roboto-Light.ttf");
        LightItalic = Typeface.createFromAsset(mManager, "fonts/Roboto-LightItalic.ttf");
        Medium = Typeface.createFromAsset(mManager, "fonts/Roboto-Medium.ttf");
        MediumItalic = Typeface.createFromAsset(mManager, "fonts/Roboto-MediumItalic.ttf");
        Regular = Typeface.createFromAsset(mManager, "fonts/Roboto-Regular.ttf");
        Thin = Typeface.createFromAsset(mManager, "fonts/Roboto-Thin.ttf");
        ThinItalic = Typeface.createFromAsset(mManager, "fonts/Roboto-ThinItalic.ttf");
        Lobster = Typeface.createFromAsset(mManager, "fonts/Lobster.ttf");
    }

}
