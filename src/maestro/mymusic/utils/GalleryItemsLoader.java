package maestro.mymusic.utils;

import android.os.AsyncTask;
import android.view.View;

import java.util.HashMap;

/**
 * Created by artyom on 7/24/14.
 */
public class GalleryItemsLoader {

    private static volatile GalleryItemsLoader instance;

    public static synchronized GalleryItemsLoader getInstance() {
        GalleryItemsLoader localInstance = instance;
        if (localInstance == null) {
            synchronized (GalleryItemsLoader.class) {
                localInstance = instance;
                if (localInstance == null) {
                    localInstance = instance = new GalleryItemsLoader();
                }
            }
        }
        return localInstance;
    }

    private HashMap<Object, Object> mCache = new HashMap<Object, Object>();

    public interface OnGalleryItemsLoaderEvent {
        public void onGalleryItemsLoaderEvent(Object result);
    }

    public interface GalleryItemsLoaderMaker {
        public Object load(Object object);
    }

    public void load(Object object, View view, OnGalleryItemsLoaderEvent listener,
                     GalleryItemsLoaderMaker maker, boolean fresh) {
        if (object != null && view != null && listener != null && maker != null) {
            if (!fresh && mCache.containsKey(object)) {
                listener.onGalleryItemsLoaderEvent(mCache.get(object));
            } else if (tryCancel(object, view)) {
                ItemsLoader loader = new ItemsLoader(object, listener, maker);
                view.setTag(loader);
                loader.execute();
            }
        }
    }

    public boolean tryCancel(Object object, View view) {
        Object tag = view.getTag();
        if (tag != null && tag instanceof ItemsLoader) {
            ItemsLoader loader = (ItemsLoader) tag;
            if (loader.KeyObject != null && !loader.KeyObject.equals(object)) {
                loader.cancel(true);
                return true;
            }
            return false;
        }
        return true;
    }

    private class ItemsLoader extends AsyncTask<Object, View, Object> {

        public Object KeyObject;
        private OnGalleryItemsLoaderEvent listener;
        private GalleryItemsLoaderMaker maker;

        public ItemsLoader(Object object, OnGalleryItemsLoaderEvent listener, GalleryItemsLoaderMaker maker) {
            this.KeyObject = object;
            this.listener = listener;
            this.maker = maker;
        }

        @Override
        protected Object doInBackground(Object... objects) {
            try {
                Object result = maker != null ? maker.load(KeyObject) : null;
                if (result != null) {
                    if (mCache.containsKey(KeyObject))
                        mCache.remove(KeyObject);
                    mCache.put(KeyObject, result);
                }
                return result;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object result) {
            super.onPostExecute(result);
            if (!isCancelled() && listener != null) {
                listener.onGalleryItemsLoaderEvent(result);
            }
        }
    }

}
