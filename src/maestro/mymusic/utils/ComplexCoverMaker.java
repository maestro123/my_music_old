package maestro.mymusic.utils;

import android.content.Context;
import android.graphics.*;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import com.dream.android.mim.MIM2;
import com.dream.android.mim.MIMDefaultMaker;
import com.dream.android.mim.MIMInternetMaker;
import com.msoft.android.mplayer.lib.MediaStoreHelper;
import com.msoft.android.mplayer.lib.models.Artist;
import com.msoft.android.mplayer.lib.models.Genre;
import com.msoft.android.mplayer.lib.models.Song;
import maestro.lastfm.lib.BaseRequestHelper;
import maestro.lastfm.lib.LastFmApi;
import maestro.lastfm.lib.LastFmArtist;
import maestro.lastfm.lib.LastFmImage;
import maestro.mymusic.R;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by artyom on 7/25/14.
 */
public class ComplexCoverMaker extends MIMDefaultMaker {

    public static final String TAG = ComplexCoverMaker.class.getSimpleName();

    private MIMInternetMaker mimInternetMaker = new MIMInternetMaker(true);
    private AlbumCoverMaker mCoverMaker = new AlbumCoverMaker(null);
    private Paint mPaint = new Paint();

    {
        mPaint.setAntiAlias(true);
    }

    @Override
    public Bitmap getBitmap(MIM2.ImageLoadObject loadObject, Context ctx) {
        Object object = loadObject.getObj();
        Song[] songs = null;
        if (object instanceof Artist) {
            Artist artist = (Artist) object;
            String url = LastFmApi.buildArtistInfoRequest(artist.Title);
            Object response = LastFmApi.getArtistInfo(url, null, false);
            if (response instanceof String || response instanceof LastFmArtist) {
                try {
                    LastFmArtist lastFmArtist = response instanceof String ? new LastFmArtist(new JSONObject((String) response))
                            : (LastFmArtist) response;
                    LastFmImage image = lastFmArtist.getImage(LastFmApi.IMAGE_EXTRA_LARGE);
                    if (image != null) {
                        return loadObject.postProcess(mimInternetMaker.getBitmap(image.getUrl(),
                                loadObject.getWidth(), loadObject.getHeight()));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else if (object instanceof Genre) {
            songs = ((Genre) object).Songs;
        }
        if (songs == null) return null;

        final int width = loadObject.getWidth();
        final int height = loadObject.getHeight();

        ArrayList<Bitmap> bitmaps = new ArrayList<Bitmap>();
        for (Song song : songs) {
            String key = song.Title + "_complex_" + song.Duration;
            MIM2.ImageLoadObject bitmapObject = new MIM2.ImageLoadObject(key, key).setObj(song)
                    .setWidth(width / 4).setHeight(height);
            Bitmap songBitmap = mCoverMaker.getBitmap(bitmapObject, ctx);
            if (songBitmap != null) {
                bitmaps.add(songBitmap);
            }
            if (bitmaps.size() == 4) break;
        }
        if (bitmaps.size() == 0) {
            return null;
        }
        final int bitmapsSize = bitmaps.size();

        if (bitmapsSize == 1) {
            return bitmaps.get(0);
        }

        final Bitmap coverBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        final Canvas canvas = new Canvas(coverBitmap);
        final Rect src = new Rect(0, 0, width / bitmapsSize, height);
        int left = 0;

        for (Bitmap bitmap : bitmaps) {
            boolean needRecycle = false;
            Bitmap bitmapToDraw = bitmap;
            if (bitmapToDraw.getHeight() < height) {
                final float fix = (float) height / bitmap.getHeight();
                final int outHeight = Math.round(bitmap.getHeight() * fix);
                final int outWidth = Math.round(bitmap.getWidth() * fix);
                bitmapToDraw = Bitmap.createScaledBitmap(bitmap, outWidth, outHeight, true);
                needRecycle = true;
            }
            canvas.drawBitmap(bitmapToDraw, left, 0, mPaint);
            left += width / bitmapsSize;
            if (needRecycle) {
                bitmapToDraw.recycle();
            }
        }

        for (Bitmap bitmap : bitmaps) {
            bitmap.recycle();
        }

        return loadObject.postProcess(coverBitmap);
    }
}
