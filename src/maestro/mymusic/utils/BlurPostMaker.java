package maestro.mymusic.utils;

import android.graphics.Bitmap;
import android.util.Log;
import com.dream.android.mim.MIM2;
import com.dream.android.mim.MIMAbstractPostMaker;
import com.dream.android.mim.MIMUtils;

/**
 * Created by Artyom on 09.08.2014.
 */
public class BlurPostMaker extends MIMAbstractPostMaker {
    @Override
    public Bitmap processBitmap(MIM2.ImageLoadObject object, Bitmap bitmap) {
        return MIMUtils.fastblur(bitmap, 6);
    }
}
