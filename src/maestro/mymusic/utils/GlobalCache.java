package maestro.mymusic.utils;

import java.util.HashMap;

/**
 * Created by artyom on 8/30/14.
 */
public class GlobalCache {

    private static volatile GlobalCache instance;

    public static final synchronized GlobalCache getInstance() {
        GlobalCache localInstance = instance;
        if (localInstance == null) {
            synchronized (GlobalCache.class) {
                if (localInstance == null) {
                    localInstance = instance = new GlobalCache();
                }
            }
        }
        return localInstance;
    }

    GlobalCache() {
    }

    private HashMap<Object, Object> mCache = new HashMap<Object, Object>();

    public Object getCache(Object key) {
        return mCache.get(key);
    }

    public void addCache(Object key, Object cache) {
        mCache.put(key, cache);
    }

}
