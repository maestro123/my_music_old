package maestro.mymusic.utils;

import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.view.View;
import android.widget.ImageView;
import com.dream.android.mim.MIMAbstractDisplayer;

/**
 * Created by artyom on 8/22/14.
 */
public class MSlideDownDisplayer extends MIMAbstractDisplayer {
    @Override
    public void display(ImageView img) {
        display(img);
    }

    public void display(View view) {
        ObjectAnimator anim = ObjectAnimator.ofFloat(view, "translationY", 0, view.getHeight());
        anim.setDuration(300);
        anim.start();
    }

}
