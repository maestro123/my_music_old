package maestro.mymusic.utils;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.msoft.android.mplayer.lib.models.Playlist;
import maestro.mymusic.R;
import maestro.mymusic.utils.adapters.BaseUpdateAdapter;

/**
 * Created by artyom on 8/30/14.
 */
public class PlaylistsAdapter extends BaseUpdateAdapter<Playlist> {

    public PlaylistsAdapter(Context context) {
        super(context);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View v, ViewGroup viewGroup) {
        Holder h;
        if (v == null) {
            h = new Holder();
            v = mInflater.inflate(R.layout.playlist_item_view, null);
            h.Title = (TextView) v.findViewById(R.id.title);
            h.Additional = (TextView) v.findViewById(R.id.additional);
            v.setTag(h);
        } else {
            h = (Holder) v.getTag();
        }
        Playlist playlist = getItem(position);
        h.Title.setText(playlist.Title);
        h.Additional.setText(playlist.getCount() + " " + mContext.getString(R.string.tracks));
        return v;
    }

    class Holder {
        TextView Title, Additional;
    }

}
