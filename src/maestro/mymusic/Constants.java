package maestro.mymusic;

/**
 * Created by artyom on 7/9/14.
 */
public class Constants {

    public static final String NAVIGATION_MIM = "navigation_mim";
    public static final String ALBUM_COVER_MIM = "album_cover_mim";
    public static final String SONG_COVER_MIM = "song_cover_mim";
    public static final String GENRE_COVER_MAKER = "genre_cover_mim";
    public static final String BACKGROUND_MIM = "background_mim";
    public static final String MUSIC_PREVIEW_MIM = "music_preview_mim";
    public static final String COMPLEX_MIM = "complex_mim";
    public static final String NETWORK_MIM = "network_mim";

    public static final String ALBUM_COLOR_CACHE = "album_color_cache";
}
