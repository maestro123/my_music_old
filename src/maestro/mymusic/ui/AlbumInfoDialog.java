package maestro.mymusic.ui;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Html;
import android.view.*;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.dream.android.mim.MIM2;
import com.dream.android.mim.MIMInternetMaker;
import com.dream.android.mim.MIMManager;
import com.msoft.android.mplayer.lib.models.Album;
import maestro.lastfm.lib.BaseRequestHelper;
import maestro.lastfm.lib.LastFmAlbum;
import maestro.lastfm.lib.LastFmApi;
import maestro.lastfm.lib.LastFmImage;
import maestro.mymusic.Constants;
import maestro.mymusic.R;
import maestro.mymusic.utils.Typefacer;

/**
 * Created by artyom on 8/27/14.
 */
public class AlbumInfoDialog extends BaseFragment implements BaseRequestHelper.RequestListener {

    public static final String TAG = AlbumInfoDialog.class.getSimpleName();

    private static final String PARAM_ALBUM = "param_album";

    private TextView mTextView;
    private ProgressBar mProgress;
    private ImageView mImage;

    private MIM2 mim;

    public static final AlbumInfoDialog makeInstance(Album album) {
        AlbumInfoDialog dialog = new AlbumInfoDialog();
        Bundle args = new Bundle(1);
        args.putParcelable(PARAM_ALBUM, album);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mim = MIMManager.getInstance().getMIM(Constants.NETWORK_MIM);
        if (mim == null) {
            mim = new MIM2(getActivity().getApplicationContext()).setMaker(new MIMInternetMaker(false));
            MIMManager.getInstance().addMIM(Constants.NETWORK_MIM, mim);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.album_info_view, null);
        mTextView = (TextView) v.findViewById(R.id.text);
        mProgress = (ProgressBar) v.findViewById(R.id.progress);
        mImage = (ImageView) v.findViewById(R.id.image);

        mTextView.setTypeface(Typefacer.Light);

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Album album = getArguments().getParcelable(PARAM_ALBUM);
        String url = LastFmApi.buildAlbumInfoRequest(album.Title, album.author);
        LastFmApi.getAlbumInfo(url, this, true);
    }

    @Override
    public void onResult(final Object result) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgress.setVisibility(View.GONE);
                if (result instanceof LastFmAlbum) {
                    final LastFmAlbum album = (LastFmAlbum) result;
                    if (album.getWiki() != null) {
                        mTextView.setText(Html.fromHtml(album.getWiki().getContent()));
                    } else {
                        mTextView.setText("No wiki data...");
                    }
                    int width = mImage.getWidth();
                    if (width > 0) {
                        loadImage(album, width, mImage.getHeight());
                    } else {
                        final ViewTreeObserver observer = mImage.getViewTreeObserver();
                        if (observer != null) {
                            observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                                @Override
                                public boolean onPreDraw() {
                                    loadImage(album, mImage.getWidth(), mImage.getHeight());
                                    mImage.getViewTreeObserver().removeOnPreDrawListener(this);
                                    return false;
                                }
                            });
                        }
                    }
                } else {
                    mTextView.setText(result.toString());
                }
            }
        });
    }

    @Override
    public boolean canDoRequest() {
        return true;
    }

    private final void loadImage(final LastFmAlbum album, int width, int height) {
        LastFmImage[] images = album.getImages();
        MIM2.ImageLoadObject loadObject = new MIM2.ImageLoadObject(images[images.length - 1].getUrl(), images[images.length - 1].getUrl());
        mim.loadImage(loadObject, mImage);
    }

}