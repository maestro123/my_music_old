package maestro.mymusic.ui;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import maestro.mymusic.R;
import maestro.mymusic.utils.adapters.BaseUpdateAdapter;
import maestro.mymusic.views.DirectionListView;

/**
 * Created by artyom on 7/9/14.
 */
public abstract class BaseListFragment<T> extends BaseFragment implements LoaderManager.LoaderCallbacks<T[]>,
        AbsListView.OnScrollListener, AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    public static final String TAG = BaseListFragment.class.getSimpleName();

    private DirectionListView mList;
    private ImageView mBackgroundImage;
    private BaseUpdateAdapter mAdapter;
    private AbsListView.OnScrollListener mScrollListener;

    private boolean actionbarToggleEnable = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.base_fragment_view, null);
        mList = (DirectionListView) v.findViewById(R.id.base_fragment_list);
        mList.setDividerHeight(0);
        mList.setOnItemClickListener(this);
        mList.setOnItemLongClickListener(this);
        mList.setOnScrollListener(this);
        mList.setFastScrollEnabled(true);
        mBackgroundImage = (ImageView) v.findViewById(R.id.background_image);
        return v;
    }

    public void setAdapter(BaseUpdateAdapter adapter) {
        mAdapter = adapter;
        if (mList != null) {
            mList.setAdapter(mAdapter);
        }
    }

    public void setOnScrollListener(AbsListView.OnScrollListener listener) {
        mScrollListener = listener;
    }

    public void load() {
        if (getLoaderManager().getLoader(getLoaderId()) != null) {
            getLoaderManager().restartLoader(getLoaderId(), null, this);
        } else {
            getLoaderManager().initLoader(getLoaderId(), null, this);
        }
    }

    private boolean canHideActionBar;

    @Override
    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (mScrollListener != null) {
            mScrollListener.onScroll(absListView, firstVisibleItem, visibleItemCount, totalItemCount);
        }
        if (actionbarToggleEnable) {
            if (totalItemCount > 0) {
                int top = absListView.getChildAt(0).getTop() * -1;
                canHideActionBar = firstVisibleItem == 0 && absListView.getChildAt(0) != null ? (top > getResources()
                        .getDimensionPixelSize(R.dimen.actionbar_height)) : true;
            } else
                canHideActionBar = false;
            if (canHideActionBar) {
                MainActivity.toggleControl(!mList.isScrollToBottom());
            }
        }
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {
        if (mScrollListener != null) {
            mScrollListener.onScrollStateChanged(absListView, i);
        }
    }

    @Override
    public void onLoaderReset(Loader<T[]> tLoader) {
    }

    @Override
    public void onLoadFinished(Loader<T[]> tLoader, T[] t) {

    }

    public abstract int getLoaderId();

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    public DirectionListView getList() {
        return mList;
    }

    public ImageView getBackgroundImage() {
        return mBackgroundImage;
    }

    public void setActionbarToggleEnable(boolean enable) {
        actionbarToggleEnable = enable;
    }

}
