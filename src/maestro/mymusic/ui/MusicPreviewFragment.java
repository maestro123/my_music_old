package maestro.mymusic.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.content.Loader;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.*;
import com.dream.android.mim.MIM2;
import com.dream.android.mim.MIMInternetMaker;
import com.dream.android.mim.MIMManager;
import com.msoft.android.mplayer.lib.models.*;
import com.msoft.android.service.MusicPlayerEngine;
import maestro.lastfm.lib.*;
import maestro.mymusic.Constants;
import maestro.mymusic.R;
import maestro.mymusic.utils.*;
import maestro.mymusic.utils.adapters.UniqRowAdapter;
import maestro.mymusic.views.DirectionListView;

/**
 * Created by artyom on 7/23/14.
 */
public class MusicPreviewFragment extends BaseListFragment<Song> implements DirectionListView.OnOverScrollListener,
        View.OnClickListener {

    public static final String TAG = MusicPreviewFragment.class.getSimpleName();

    private static final String SAVED_ITEM = "saved_item";

    private BaseMediaItem mItem;
    private UniqRowAdapter mAdapter;

    private View mHeaderView;
    private ImageView mHeaderNiceBack;
    private ImageView mHeaderCover;
    private TextView mHeaderTitle;
    private TextView mHeaderAdditional;
    private ImageButton btnAlbumInfo;
    private MIM2 mim;
    private MFlipDisplayer mFlipDisplayer = new MFlipDisplayer();
    private MIM2.ImageLoadObject mCurrentLoadObject, mCurrentNiceLoadObject;
    private int coverWidth, coverHeight, niceCoverWidth, niceCoverHeight, additionalSidePadding;

    private final float headerOffset = .5f;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mim = MIMManager.getInstance().getMIM(Constants.SONG_COVER_MIM);
        if (mim == null) {
            final int size = getResources().getDimensionPixelSize(R.dimen.music_preview_header_cover_image_size);
            mim = new MIM2(getActivity().getApplicationContext()).setDefaultSize(size, size);
            MIMManager.getInstance().addMIM(Constants.MUSIC_PREVIEW_MIM, mim);
        }
        coverWidth = coverHeight = getResources().getDimensionPixelSize(R.dimen.music_preview_header_cover_image_size);
        niceCoverHeight = getResources().getDimensionPixelSize(R.dimen.music_preview_header_height);
        niceCoverWidth = MainActivity.getSecondListWidth();
        additionalSidePadding = isTablet ? getResources().getDimensionPixelSize(R.dimen.song_additional_padding)
                : (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
//        setActionbarToggleEnable(is7inchTablet || !isTablet);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mHeaderView = getLayoutInflater(savedInstanceState).inflate(R.layout.music_preview_header_view, null);
        mHeaderNiceBack = (ImageView) mHeaderView.findViewById(R.id.music_preview_header_nice_back);
        mHeaderCover = (ImageView) mHeaderView.findViewById(R.id.music_preview_header_cover);
        mHeaderTitle = (TextView) mHeaderView.findViewById(R.id.music_preview_header_title);
        mHeaderAdditional = (TextView) mHeaderView.findViewById(R.id.music_preview_header_additional);

        btnAlbumInfo = (ImageButton) mHeaderView.findViewById(R.id.album_info_button);

        mHeaderTitle.setTypeface(Typefacer.Regular);
        mHeaderAdditional.setTypeface(Typefacer.Regular);

        getList().addHeaderView(mHeaderView);
        getList().setOnOverScrollListener(this);
        getList().setOverScrollHeight(0);

        btnAlbumInfo.setOnClickListener(this);

        ((RelativeLayout.LayoutParams) mHeaderCover.getLayoutParams()).leftMargin = additionalSidePadding;
        mHeaderTitle.setPadding(mHeaderTitle.getPaddingLeft(), mHeaderTitle.getPaddingTop(),
                mHeaderTitle.getPaddingRight() + additionalSidePadding, mHeaderTitle.getPaddingBottom());
        mHeaderAdditional.setPadding(mHeaderAdditional.getPaddingLeft(), mHeaderAdditional.getPaddingTop(),
                mHeaderAdditional.getPaddingRight() + additionalSidePadding, mHeaderAdditional.getPaddingBottom());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null)
            mItem = savedInstanceState.getParcelable(SAVED_ITEM);
        setAdapter(mAdapter = new UniqRowAdapter(getActivity(), !(mItem instanceof Album), additionalSidePadding
                + (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics())));
        if (mItem != null) {
            prepareUI();
            load();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        super.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
        if (mHeaderView != null && mHeaderView.getTop() > -niceCoverHeight) {
            mHeaderNiceBack.setTranslationY(mHeaderView.getTop() * -1 * headerOffset);
        }
        if (mAdapter != null)
            mAdapter.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
        super.onScrollStateChanged(view, scrollState);
        if (mAdapter != null)
            mAdapter.onScrollStateChanged(view, scrollState);
    }

    @Override
    public int getLoaderId() {
        return TAG.hashCode();
    }

    public void resetItem(BaseMediaItem item) {
        mItem = item;
        if (MainActivity != null) {
            prepareUI();
            load();
        }
    }

    public void prepareUI() {
        if (mim != null) {
            if (mCurrentLoadObject != null)
                mim.cancel(mHeaderCover, mCurrentLoadObject.getKey());
            if (mCurrentNiceLoadObject != null)
                mim.cancel(mHeaderNiceBack, mCurrentNiceLoadObject.getKey());
        }

        if (mItem instanceof Album) {
            final Album album = (Album) mItem;
            mHeaderTitle.setText(album.Title);
            mHeaderAdditional.setText(album.author);

            mim = MIMManager.getInstance().getMIM(Constants.ALBUM_COVER_MIM);
            if (mim == null) {
                mim = new MIM2(getActivity().getApplicationContext())
                        .setMaker(new AlbumCoverMaker(getActivity().getApplicationContext()));
                MIMManager.getInstance().addMIM(Constants.ALBUM_COVER_MIM, mim);
            }

            String key = album.author + album.Title + "_" + coverWidth + "_" + coverHeight;
            mCurrentLoadObject = new MIM2.ImageLoadObject(key, key)
                    .setWidth(coverWidth).setHeight(coverHeight).setObj(album).setUseDisplayer(true);

            key = album.author + album.Title + "_" + niceCoverWidth + "_" + niceCoverHeight;
            mCurrentNiceLoadObject = new MIM2.ImageLoadObject(key, key)
                    .setWidth(niceCoverWidth).setHeight(niceCoverHeight).setPostMaker(new BlurPostMaker())
                    .setObj(album).setUseDisplayer(true);

            LastFmApi.getAlbumInfo(LastFmApi.buildAlbumInfoRequest(album.Title, album.author),
                    new BaseRequestHelper.RequestListener() {
                        @Override
                        public void onResult(Object result) {
                            if (getActivity() != null) {
                                if (result instanceof LastFmAlbum) {
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            btnAlbumInfo.setVisibility(View.VISIBLE);
                                            mFlipDisplayer.display(btnAlbumInfo);
                                        }
                                    });
                                }
                            }
                        }

                        @Override
                        public boolean canDoRequest() {
                            return true;
                        }
                    }, true);
        } else if (mItem instanceof Genre) {
            Genre genre = (Genre) mItem;
            mHeaderTitle.setText(genre.Title);
            mHeaderAdditional.setText(genre.getCount() + " " + getString(R.string.tracks));

            mim = MIMManager.getInstance().getMIM(Constants.COMPLEX_MIM);
            if (mim == null) {
                mim = new MIM2(getActivity().getApplicationContext())
                        .setDisplayAnimationEnable(true)
                        .setDisplayFromCacheEnable(true)
                        .setMaker(new ComplexCoverMaker());
                MIMManager.getInstance().addMIM(Constants.COMPLEX_MIM, mim);
            }
            String key = genre.Title + "_" + coverWidth + "_" + coverHeight;
            mCurrentLoadObject = new MIM2.ImageLoadObject(key, key)
                    .setObj(genre).setWidth(coverWidth).setHeight(coverHeight);

            key = genre.Title + "_" + niceCoverWidth + "_" + niceCoverHeight;
            mCurrentNiceLoadObject = new MIM2.ImageLoadObject(key, key)
                    .setWidth(niceCoverWidth).setHeight(niceCoverHeight).setPostMaker(new BlurPostMaker())
                    .setObj(genre).setUseDisplayer(true);

        } else if (mItem instanceof Artist) {
            Artist artist = (Artist) mItem;
            mHeaderTitle.setText(artist.Title);
            mHeaderAdditional.setText(artist.Count + " " + getString(R.string.tracks));
            LastFmApi.getArtistInfo(LastFmApi.buildArtistInfoRequest(artist.Title),
                    new BaseRequestHelper.RequestListener() {
                        @Override
                        public void onResult(Object result) {
                            if (getActivity() != null) {
                                if (result instanceof LastFmArtist) {
                                    LastFmArtist lastFmArtist = (LastFmArtist) result;
                                    LastFmImage image = lastFmArtist.getImage(LastFmApi.IMAGE_EXTRA_LARGE);
                                    if (image != null) {
                                        mCurrentNiceLoadObject = new MIM2.ImageLoadObject(image.getUrl(), image.getUrl())
                                                .setWidth(niceCoverWidth).setHeight(niceCoverHeight)
                                                .setMaker(new MIMInternetMaker(false))
                                                .setPostMaker(new BlurPostMaker()).setUseDisplayer(true);
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                mim.loadImage(mCurrentNiceLoadObject, mHeaderNiceBack);
                                            }
                                        });
                                    }

                                    image = lastFmArtist.getImage(LastFmApi.IMAGE_LARGE);
                                    if (image != null) {
                                        mCurrentLoadObject = new MIM2.ImageLoadObject(image.getUrl(), image.getUrl())
                                                .setWidth(coverWidth)
                                                .setHeight(coverHeight)
                                                .setMaker(new MIMInternetMaker(false));
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                mim.loadImage(mCurrentLoadObject, mHeaderCover);
                                            }
                                        });
                                    }

                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            btnAlbumInfo.setVisibility(View.VISIBLE);
                                        }
                                    });
                                }
                            }
                        }

                        @Override
                        public boolean canDoRequest() {
                            return true;
                        }
                    }, true);
        } else if (mItem instanceof Playlist) {
            Playlist playlist = (Playlist) mItem;
            mHeaderTitle.setText(playlist.Title);
            mHeaderAdditional.setText(playlist.getCount() + " " + getString(R.string.tracks));
        }

        if (mim != null) {
            if (mCurrentLoadObject != null) {
                mCurrentLoadObject.setDisplayer(mFlipDisplayer);
                mim.loadImage(mCurrentLoadObject, mHeaderCover);
            }
            if (mCurrentNiceLoadObject != null) {
                mCurrentNiceLoadObject.setDisplayer(mFlipDisplayer);
                mim.loadImage(mCurrentNiceLoadObject, mHeaderNiceBack);
            }
        }

    }

    @Override
    public Loader<Song[]> onCreateLoader(int i, Bundle bundle) {
        return new Loaders.SongsLoader(getActivity(), mItem);
    }

    @Override
    public void onLoadFinished(Loader<Song[]> loader, Song[] songs) {
        if (mAdapter != null)
            mAdapter.update(songs);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        super.onItemClick(adapterView, view, i, l);
        MusicPlayerEngine.getInstance().play((Song[]) mAdapter.getItems(), i - 1);
        MainActivity.openMusicPlayFragment();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.album_info_button:
                AlbumInfoDialog dialog = AlbumInfoDialog.makeInstance((Album) mItem);
                dialog.show(getChildFragmentManager(), AlbumInfoDialog.TAG);
                break;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(SAVED_ITEM, mItem);
    }

    @Override
    public void onOverScroll(int scrollX, int scrollY, boolean clampedX, boolean clampedY) {
    }
}
