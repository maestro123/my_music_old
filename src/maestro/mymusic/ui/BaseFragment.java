package maestro.mymusic.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.Window;
import maestro.mymusic.Main;
import maestro.mymusic.R;

/**
 * Created by artyom on 7/9/14.
 */
public class BaseFragment extends DialogFragment {

    public Main MainActivity;

    public boolean isTablet;
    public boolean is7inchTablet;
    public boolean is10inchTablet;
    public boolean isLandscape;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        is7inchTablet = getResources().getBoolean(R.bool.is7inch);
        is10inchTablet = getResources().getBoolean(R.bool.is10inch);
        isTablet = is10inchTablet || is7inchTablet;
        isLandscape = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof Main) {
            MainActivity = (Main) activity;
            MainActivity.toggleControl(true);
        }
    }

    @Override
    public void onDetach() {
        MainActivity = null;
        super.onDetach();
    }
}
