package maestro.mymusic.ui;

import android.os.Bundle;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.AdapterView;
import com.msoft.android.mplayer.lib.models.Genre;
import maestro.mymusic.Main;
import maestro.mymusic.utils.Loaders;
import maestro.mymusic.utils.adapters.SimpleGenreAdapter;

/**
 * Created by artyom on 7/11/14.
 */
public class GenresFragment extends BaseListFragment<Genre> {

    private SimpleGenreAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionbarToggleEnable(!isTablet);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setAdapter(mAdapter = new SimpleGenreAdapter(getActivity(), MainActivity.getMainListWidth()));
        setOnScrollListener(mAdapter);
        load();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        super.onItemClick(adapterView, view, i, l);
        if (MainActivity != null)
            MainActivity.openMusicPreviewFragment(mAdapter.getItem(i));
    }

    @Override
    public int getLoaderId() {
        return Main.MENU_ITEM_GENRES;
    }

    @Override
    public Loader<Genre[]> onCreateLoader(int i, Bundle bundle) {
        return new Loaders.GenresLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<Genre[]> tLoader, Genre[] t) {
        super.onLoadFinished(tLoader, t);
        mAdapter.update(t);
    }

}
