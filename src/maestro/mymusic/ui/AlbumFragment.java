package maestro.mymusic.ui;

import android.os.Bundle;
import android.support.v4.content.Loader;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import com.msoft.android.mplayer.lib.models.Album;
import maestro.mymusic.Main;
import maestro.mymusic.utils.SettingsHolder;
import maestro.mymusic.utils.adapters.AlbumAdapter;
import maestro.mymusic.utils.adapters.AlbumNormalGridAdapter;
import maestro.mymusic.utils.adapters.BaseUpdateAdapter;
import maestro.mymusic.utils.Loaders;
import maestro.mymusic.utils.adapters.MGridAdapter;

/**
 * Created by artyom on 7/10/14.
 */
public class AlbumFragment extends BaseListFragment implements MGridAdapter.OnItemClickListener {

    public static final String TAG = AlbumFragment.class.getSimpleName();

    private BaseUpdateAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionbarToggleEnable(!isTablet);
        setHasOptionsMenu(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        applyAdapter();
    }

    private final void applyAdapter() {
        final int viewType = SettingsHolder.getInstance().getPrefAlbumViewType();
        switch (viewType) {
            case 1:
                mAdapter = new AlbumNormalGridAdapter(getActivity(), MainActivity.getMainListWidth(), getNumOfColumns(), BaseUpdateAdapter.DEF_PADDING);
                ((AlbumNormalGridAdapter) mAdapter).setOnItemClickListener(this);
                break;
            case 2:

                break;
            default:
                mAdapter = new AlbumAdapter(getActivity(), MainActivity.getMainListWidth(), isTablet
                        ? 0 : BaseUpdateAdapter.DEF_PADDING);
                ((AlbumAdapter) mAdapter).setOnItemClickListener(this);
                break;
        }
        setAdapter(mAdapter);
        setOnScrollListener(mAdapter);
        load();
    }

    private int getNumOfColumns() {
        final float size160 = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                160, getResources().getDisplayMetrics());
        return !isTablet && !isLandscape ? 2 : Math.round(MainActivity.getMainListWidth() / size160);
    }

    @Override
    public int getLoaderId() {
        return Main.MENU_ITEM_ALBUMS;
    }

    @Override
    public Loader onCreateLoader(int i, Bundle bundle) {
        return new Loaders.AlbumLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader loader, Object[] o) {
        mAdapter.update(o);
    }

    @Override
    public void onItemClick(AdapterView adapterView, View view, int i, long l) {
        super.onItemClick(adapterView, view, i, l);
        Album album = (Album) adapterView.getItemAtPosition(i);
        if (MainActivity != null)
            MainActivity.openMusicPreviewFragment(album);
    }

    @Override
    public boolean onItemLongClick(AdapterView adapterView, View view, int i, long l) {
        return super.onItemLongClick(adapterView, view, i, l);
    }

    @Override
    public void onItemClick(View v, int position) {
        if (MainActivity != null)
            MainActivity.openMusicPreviewFragment((Album) mAdapter.getItem(position));
    }

    @Override
    public boolean onItemLongClick(View v, int position) {
        return false;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.add("0");
        menu.add("1");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle().equals("0")) {
            SettingsHolder.getInstance().setPrefAlbumViewType(0);
            applyAdapter();
            return true;
        } else if (item.getTitle().equals("1")) {
            SettingsHolder.getInstance().setPrefAlbumViewType(1);
            applyAdapter();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
