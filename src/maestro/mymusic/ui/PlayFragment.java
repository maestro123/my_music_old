package maestro.mymusic.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.*;
import android.widget.*;
import com.dream.android.mim.MIM2;
import com.dream.android.mim.MIMManager;
import com.msoft.android.helpers.MFormatter;
import com.msoft.android.mplayer.lib.models.Song;
import com.msoft.android.service.MusicPlayerEngine;
import maestro.mymusic.Constants;
import maestro.mymusic.R;
import maestro.mymusic.utils.SettingsHolder;
import maestro.mymusic.views.dropmenu.BDropMenu;
import maestro.mymusic.views.dropmenu.DropMenu;
import maestro.mymusic.utils.AlbumCoverMaker;
import maestro.mymusic.utils.MViewPager;
import maestro.mymusic.utils.adapters.UniqRowAdapter;

/**
 * Created by artyom on 8/23/14.
 */
public class PlayFragment extends BaseFragment implements MusicPlayerEngine.MusicPlayerListener,
        View.OnClickListener, ViewPager.OnPageChangeListener, SeekBar.OnSeekBarChangeListener,
        MViewPager.ExternalTouchListener, AdapterView.OnItemClickListener, SettingsHolder.OnUIPreferenceChangeListener {

    public static final String TAG = PlayFragment.class.getSimpleName();

    private MViewPager mPager;
    private RelativeLayout mParent;
    private FrameLayout mParentFrame;
    private ImageButton btnPrev, btnPlay, btnNext, btnPause,
            btnShuffleOn, btnShuffleOff, btnLoop, btnLoopSingle, btnLoopOff, btnShowSongs;
    private SeekBar mSeekBar;
    private TextView txtTimeLeft, txtTimeTotal;
    private ViewFlipper mControlFlipper, mShuffleFlipper, mLoopFlipper;
    private ListView mSongsList;
    private RelativeLayout mControlParent;
    private PlayFragmentPagerAdapter mAdapter;
    private UniqRowAdapter mSongsAdapter;

    private MusicPlayerEngine mEngine = MusicPlayerEngine.getInstance();

    private boolean isSeekBarOnTouch = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.play_fragment_view, null);
        mParentFrame = (FrameLayout) v;
        mParent = (RelativeLayout) v.findViewById(R.id.play_fragment_parent);
        mControlParent = (RelativeLayout) v.findViewById(R.id.play_fragment_control_layout);
        mPager = (MViewPager) v.findViewById(R.id.pager);
        btnPrev = (ImageButton) v.findViewById(R.id.prev_button);
        btnPlay = (ImageButton) v.findViewById(R.id.play_button);
        btnPause = (ImageButton) v.findViewById(R.id.pause_button);
        btnNext = (ImageButton) v.findViewById(R.id.next_button);
        btnShuffleOn = (ImageButton) v.findViewById(R.id.shuffle_on);
        btnShuffleOff = (ImageButton) v.findViewById(R.id.shuffle_off);
        btnLoop = (ImageButton) v.findViewById(R.id.loop_on);
        btnLoopSingle = (ImageButton) v.findViewById(R.id.loop_single);
        btnLoopOff = (ImageButton) v.findViewById(R.id.loop_off);
        btnShowSongs = (ImageButton) v.findViewById(R.id.show_songs_list_button);
        mControlFlipper = (ViewFlipper) v.findViewById(R.id.control_flipper);
        mShuffleFlipper = (ViewFlipper) v.findViewById(R.id.shuffle_flipper);
        mLoopFlipper = (ViewFlipper) v.findViewById(R.id.loop_flipper);
        mSeekBar = (SeekBar) v.findViewById(R.id.play_progress);
        txtTimeLeft = (TextView) v.findViewById(R.id.time_left);
        txtTimeTotal = (TextView) v.findViewById(R.id.time_total);
        mSongsList = (ListView) v.findViewById(R.id.play_songs_list);
        if (mSongsList != null) {
            mSongsList.setDividerHeight(0);
            mSongsList.setOnItemClickListener(this);
        }
        btnPrev.setOnClickListener(this);
        btnPlay.setOnClickListener(this);
        btnPause.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        btnShuffleOn.setOnClickListener(this);
        btnShuffleOff.setOnClickListener(this);
        btnLoop.setOnClickListener(this);
        btnLoopSingle.setOnClickListener(this);
        btnLoopOff.setOnClickListener(this);
        if (btnShowSongs != null) {
            btnShowSongs.setOnClickListener(this);
        }

        mPager.setOnPageChangeListener(this);
        mSeekBar.setOnSeekBarChangeListener(this);

        v.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                return true;
            }
        });
        mPager.setExternalTouchListener(this);

        final int primaryColor = SettingsHolder.getInstance().getPrimaryColor();

        mControlParent.setBackgroundColor(primaryColor);
        mSongsList.setBackgroundColor(primaryColor);

        prepareDropMenu(v);
        ensureLoopButtonsState();
        ensureShuffleButtonsState();

        return v;
    }

    @Override
    public void onUIPreferenceChange(String key, Object value) {
        if (key.equals(SettingsHolder.PREF_PRIMARY_COLOR)) {
            if (mControlParent != null)
                mControlParent.setBackgroundColor((Integer) value);
            if (mSongsList != null)
                mSongsList.setBackgroundColor((Integer) value);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        SettingsHolder.getInstance().attachOnUIPreferenceChangeListener(TAG, this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        SettingsHolder.getInstance().detachOnUIPreferenceChangeListener(TAG);
    }

    @Override
    public void onStart() {
        super.onStart();
        MainActivity.setActionBarAlpha(0);
    }

    @Override
    public void onStop() {
        super.onStop();
        MainActivity.setActionBarAlpha(255);
    }

    @Override
    public void onResume() {
        super.onResume();
        mEngine.attachListener(TAG, this);
        if (mEngine.canPlay()) {
            applyAdapter();
            updateSong();
            updateSongPlayingPosition();
        }
    }

    @Override
    public void onPause() {
        mEngine.detachListener(TAG);
        super.onPause();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPageScrolled(int i, float v, int i2) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    @Override
    public void onPageSelected(final int i) {
        if (i != mEngine.getCurrentPlayingSongPosition())
            new Thread() {
                @Override
                public void run() {
                    super.run();
                    try {
                        sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    mEngine.setCurrentPlayingPosition(i);
                }
            }.start();
    }

    @Override
    public void onMusicPlayerEvent(MusicPlayerEngine.MUSIC_PLAYER_EVENT event) {
        switch (event) {
            case START:
//                mSeekBar.setProgress(0);
                updateSong();
                updateSongPlayingPosition();
                break;
            case TIME_UPDATE:
                if (mEngine.isPlaying())
                    updateSongPlayingPosition();
                break;
            case SONGS_CHANGE:
                applyAdapter();
                break;
            case PAUSE:
            case RESUME:
                updateButtonState();
                updateSongPlayingPosition();
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(View view) {
        final int id = view.getId();
        switch (id) {
            case R.id.prev_button:
                mEngine.prev();
                break;
            case R.id.pause_button:
            case R.id.play_button:
                if (mEngine.canPlay()) {
                    if (mEngine.isPlaying()) {
                        mEngine.pause();
                    } else {
                        mEngine.resume();
                    }
                }
                break;
            case R.id.next_button:
                mEngine.next();
                break;
            case R.id.shuffle_on:
            case R.id.shuffle_off:
                mEngine.setShuffle(!mEngine.getShuffle());
                ensureShuffleButtonsState();
                break;
            case R.id.loop_on:
                mEngine.setLoop(MusicPlayerEngine.LOOP_STATE.SINGLE_LOOP);
                ensureLoopButtonsState();
                break;
            case R.id.loop_single:
                mEngine.setLoop(MusicPlayerEngine.LOOP_STATE.NO_LOOP);
                ensureLoopButtonsState();
                break;
            case R.id.loop_off:
                mEngine.setLoop(MusicPlayerEngine.LOOP_STATE.FULL_LOOP);
                ensureLoopButtonsState();
                break;
            case R.id.show_songs_list_button:
                if (mSongsList.getVisibility() == View.VISIBLE) {
                    MainActivity.getActionBar().hide();
                    mSongsList.setPivotX(0);
                    mSongsList.setPivotY(mSongsList.getBottom());
                    mSongsList.animate().scaleX(0.0f).scaleY(0.0f).setDuration(300)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    mSongsList.setVisibility(View.GONE);
                                }
                            }).start();
                } else {
                    MainActivity.getActionBar().hide();
                    mSongsList.setPivotX(0);
                    mSongsList.setPivotY(mSongsList.getBottom());
                    mSongsList.setScaleX(0f);
                    mSongsList.setScaleY(0f);
                    mSongsList.animate().scaleX(1.0f).scaleY(1.0f).setDuration(300)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationStart(Animator animation) {
                                    super.onAnimationStart(animation);
                                    mSongsList.setVisibility(View.VISIBLE);
                                }
                            }).start();
                }
                break;
        }
    }


    private void applyAdapter() {
        mPager.setAdapter(mAdapter = new PlayFragmentPagerAdapter(getChildFragmentManager()));
        if (mSongsList != null) {
            mSongsList.setAdapter(mSongsAdapter = new UniqRowAdapter(getActivity()).setIsForPlayView(true));
            mSongsAdapter.update(mEngine.getCurrentPlayingSongs());
            mSongsList.setOnScrollListener(mSongsAdapter);
        }
    }

    private final void updateSong() {
        mPager.setCurrentItem(mEngine.getCurrentPlayingSongPosition(), false);
        updateButtonState();
        mSeekBar.setMax(mEngine.getDuration() / 1000);
        if (mSongsAdapter != null)
            mSongsAdapter.notifyDataSetInvalidated();
    }

    private final void updateSongPlayingPosition() {
        if (!isSeekBarOnTouch) {
            final int currentPlayingPosition = mEngine.getCurrentPlayingPosition();
            mSeekBar.setProgress(currentPlayingPosition / 1000);
            updateSongPlayPositionText(currentPlayingPosition, mEngine.getDuration());
        }
    }

    private final void updateSongPlayPositionText(long current, long total) {
        txtTimeLeft.setText(MFormatter.formatTimeFromMillis(current));
        txtTimeTotal.setText(MFormatter.formatTimeFromMillis(total - current));
    }

    private void updateButtonState() {
        if (mEngine.isPlaying() && !mControlFlipper.getCurrentView().equals(mControlFlipper.getChildAt(1))) {
            mControlFlipper.setDisplayedChild(1);
        } else if (!mEngine.isPlaying() && !mControlFlipper.getCurrentView().equals(mControlFlipper.getChildAt(0))) {
            mControlFlipper.setDisplayedChild(0);
        }
    }

    private void ensureLoopButtonsState() {
        if (mEngine.getLoop() == MusicPlayerEngine.LOOP_STATE.FULL_LOOP) {
            mLoopFlipper.setDisplayedChild(0);
        } else if (mEngine.getLoop() == MusicPlayerEngine.LOOP_STATE.SINGLE_LOOP) {
            mLoopFlipper.setDisplayedChild(1);
        } else {
            mLoopFlipper.setDisplayedChild(2);
        }
    }

    private void ensureShuffleButtonsState() {
        if (mEngine.getShuffle()) {
            mShuffleFlipper.setDisplayedChild(0);
        } else {
            mShuffleFlipper.setDisplayedChild(1);
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean user) {
        if (user) {
            mEngine.seekTo(progress * 1000);
            if (!mEngine.isPlaying()) {
                mEngine.resume();
            }
            updateSongPlayPositionText(progress * 1000, mEngine.getDuration());
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        isSeekBarOnTouch = true;
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        isSeekBarOnTouch = false;
    }


    private float startY = -1;
    private float prevY = -1;

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        View localView = mParent;
        final int height = localView.getHeight();
        switch (event.getAction()) {
            case MotionEvent.ACTION_MOVE:
                if (startY == -1) {
                    startY = prevY = event.getRawY();
                } else {
                    float translation = localView.getTranslationY() + (event.getRawY() - prevY) * 1.2f;
                    if (translation > 0) {
                        localView.setTranslationY(translation);
                        float alpha = translation > 0 ? translation / height : 0;
                        mParentFrame.setAlpha(1f - alpha);
                        prevY = event.getRawY();
                        MainActivity.setActionBarAlpha(Math.round(255 * alpha));
                    }
                }
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                startY = -1;
                if (localView.getTranslationY() >= height / 2) {
                    animatePlayView(localView, localView.getTranslationY(), height, localView.getAlpha(), 0f, new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            if (getActivity() != null) {
                                getActivity().getSupportFragmentManager().popBackStack();
                            }
                        }
                    });
                    animateAlpha(mParentFrame, mParentFrame.getAlpha(), 0);
                } else {
                    animatePlayView(localView, localView.getTranslationY(), 0, localView.getAlpha(), 1f, null);
                    animateAlpha(mParentFrame, mParentFrame.getAlpha(), 1f);
                    MainActivity.setActionBarAlpha(0);
                }
                break;
        }
        return true;
    }

    private final void animatePlayView(View view, float from, float to, float fromAlpha, float toAlpha,
                                       AnimatorListenerAdapter listenerAdapter) {
        AnimatorSet set = new AnimatorSet();
        ObjectAnimator aAnim = ObjectAnimator.ofFloat(view, "alpha", fromAlpha, toAlpha);
        ObjectAnimator tAnim = ObjectAnimator.ofFloat(view, "translationY", from, to);
        if (listenerAdapter != null)
            set.addListener(listenerAdapter);
        set.playTogether(aAnim, tAnim);
        set.start();
    }

    private final void animateAlpha(View view, float fromAlpha, float toAlpha) {
        ObjectAnimator aAnim = ObjectAnimator.ofFloat(view, "alpha", fromAlpha, toAlpha);
        aAnim.start();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        mEngine.play(i);
    }

    private final class PlayFragmentPagerAdapter extends FragmentPagerAdapter {

        private Song[] songs;

        public PlayFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
            songs = mEngine.getCurrentPlayingSongs();
        }

        @Override
        public Fragment getItem(int i) {
            return new SongImageFragment();
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Object object = super.instantiateItem(container, position);
            ((SongImageFragment) object).setItem(songs[position]);
            return object;
        }

        @Override
        public int getCount() {
            return songs.length;
        }
    }

    public static final class SongImageFragment extends BaseFragment implements
            SettingsHolder.OnUIPreferenceChangeListener {

        public static final String TAG = SongImageFragment.class.getSimpleName();

        private static final String PARAM_SONG = "param_song";

        private ImageView mImageView;
        private TextView txtTitle;
        private TextView tatAdditional;
        private LinearLayout mTextParent;
        private MIM2 mim;

        private Song mSong;
        private String key;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            mim = MIMManager.getInstance().getMIM(Constants.SONG_COVER_MIM);
            if (mim == null) {
                mim = new MIM2(getActivity().getApplicationContext())
                        .setMaker(new AlbumCoverMaker(getActivity().getApplicationContext()));
                MIMManager.getInstance().addMIM(Constants.SONG_COVER_MIM, mim);
            }
        }

        public void setItem(Song song) {
            mSong = song;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            final View v = inflater.inflate(R.layout.play_fragment_image_view, null);
            mImageView = (ImageView) v.findViewById(R.id.image);
            txtTitle = (TextView) v.findViewById(R.id.title);
            tatAdditional = (TextView) v.findViewById(R.id.additional);
            mTextParent = (LinearLayout) v.findViewById(R.id.text_parent);
            mTextParent.setBackgroundColor(SettingsHolder.getInstance().getPrimaryColor());
            return v;
        }

        @Override
        public void onUIPreferenceChange(String key, Object value) {
            if (key.equals(SettingsHolder.PREF_PRIMARY_COLOR)) {
                if (mTextParent != null)
                    mTextParent.setBackgroundColor((Integer) value);
            }
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            if (mSong == null && savedInstanceState != null)
                mSong = savedInstanceState.getParcelable(PARAM_SONG);
            ViewTreeObserver observer = mImageView.getViewTreeObserver();
            if (observer != null)
                observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        if (getActivity() != null) {
                            key = mSong.Title + "_song_" + mSong.Duration + "_play_"
                                    + (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ? "land" : "port");
                            MIM2.ImageLoadObject loadObject = new MIM2.ImageLoadObject(key, key).setObj(mSong)
                                    .setWidth(mImageView.getWidth()).setHeight(mImageView.getHeight()).setUseDisplayer(true);
                            mim.loadImage(loadObject, mImageView);
                            mImageView.getViewTreeObserver().removeOnPreDrawListener(this);
                        }
                        return true;
                    }
                });
            txtTitle.setText(mSong.Title);
            tatAdditional.setText(mSong.Author);
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            SettingsHolder.getInstance().attachOnUIPreferenceChangeListener(TAG, this);
        }

        @Override
        public void onDetach() {
            super.onDetach();
            SettingsHolder.getInstance().detachOnUIPreferenceChangeListener(TAG);
        }

        @Override
        public void onDestroy() {
            mim.cancel(mImageView, key);
            super.onDestroy();
        }

        @Override
        public void onSaveInstanceState(Bundle outState) {
            super.onSaveInstanceState(outState);
            outState.putParcelable(PARAM_SONG, mSong);
        }

    }

    private ImageButton btnDropMenu;
    private BDropMenu bDropMenu;

    private void prepareDropMenu(View v) {
        btnDropMenu = (ImageButton) v.findViewById(R.id.drop_menu);
        bDropMenu = (BDropMenu) v.findViewById(R.id.play_fragment_bdrop);

        final int size60dp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60, getResources().getDisplayMetrics());
        bDropMenu.addItem(new DropMenu.DropMenuItem(0, null, R.drawable.ic_attention, R.drawable.test_circle).setSize(size60dp, size60dp));
        bDropMenu.addItem(new DropMenu.DropMenuItem(0, null, R.drawable.ic_attention, R.drawable.test_circle).setSize(size60dp, size60dp));
        bDropMenu.addItem(new DropMenu.DropMenuItem(0, null, R.drawable.ic_attention, R.drawable.test_circle).setSize(size60dp, size60dp));
        bDropMenu.addItem(new DropMenu.DropMenuItem(0, null, R.drawable.ic_attention, R.drawable.test_circle).setSize(size60dp, size60dp));
        bDropMenu.addItem(new DropMenu.DropMenuItem(0, null, R.drawable.ic_attention, R.drawable.test_circle).setSize(size60dp, size60dp));

        bDropMenu.prepare();
        bDropMenu.bindClickView(btnDropMenu);
    }


}
