package maestro.mymusic.ui;

import android.os.Bundle;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.AdapterView;
import com.msoft.android.mplayer.lib.models.BaseMediaItem;
import com.msoft.android.mplayer.lib.models.Playlist;
import maestro.mymusic.Main;
import maestro.mymusic.utils.Loaders;
import maestro.mymusic.utils.PlaylistsAdapter;

/**
 * Created by artyom on 8/30/14.
 */
public class PlaylistFragment extends BaseListFragment<Playlist> {

    private PlaylistsAdapter mAdapter;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setAdapter(mAdapter = new PlaylistsAdapter(getActivity()));
    }

    @Override
    public int getLoaderId() {
        return Main.MENU_ITEM_PLAYLIST;
    }

    @Override
    public Loader<Playlist[]> onCreateLoader(int i, Bundle bundle) {
        return new Loaders.PlaylistsLoader(getActivity());
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        super.onItemClick(adapterView, view, i, l);
        if (MainActivity != null)
            MainActivity.openMusicPreviewFragment((BaseMediaItem) adapterView.getItemAtPosition(i));
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
        return super.onItemLongClick(adapterView, view, i, l);
    }
}
