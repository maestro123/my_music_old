package maestro.mymusic.ui;

import android.os.Bundle;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.AdapterView;
import com.msoft.android.mplayer.lib.models.Song;
import com.msoft.android.service.MusicPlayerEngine;
import maestro.mymusic.Main;
import maestro.mymusic.utils.Loaders;
import maestro.mymusic.utils.adapters.UniqRowAdapter;

/**
 * Created by artyom on 7/11/14.
 */
public class SongsFragment extends BaseListFragment<Song> {

    private UniqRowAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionbarToggleEnable(!isTablet);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setAdapter(mAdapter = new UniqRowAdapter(getActivity()));
        setOnScrollListener(mAdapter);
        getList().setFastScrollEnabled(true);
        load();
    }

    @Override
    public void onStart() {
        super.onStart();
        MusicPlayerEngine.getInstance().attachListener(TAG, mAdapter);
    }

    @Override
    public void onStop() {
        MusicPlayerEngine.getInstance().detachListener(TAG);
        super.onStop();
    }

    @Override
    public int getLoaderId() {
        return Main.MENU_ITEM_ALL_SONGS;
    }

    @Override
    public Loader<Song[]> onCreateLoader(int i, Bundle bundle) {
        return new Loaders.SongsLoader(getActivity(), null);
    }

    @Override
    public void onLoadFinished(Loader<Song[]> tLoader, Song[] t) {
        super.onLoadFinished(tLoader, t);
        mAdapter.update(t);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        super.onItemClick(adapterView, view, i, l);
        MusicPlayerEngine.getInstance().play((Song[]) mAdapter.getItems(), i);
        MainActivity.openMusicPlayFragment();
    }
}
