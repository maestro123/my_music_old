package maestro.mymusic.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.*;
import com.dream.android.mim.MIM2;
import com.dream.android.mim.MIMInternetMaker;
import com.dream.android.mim.MIMManager;
import com.msoft.android.mplayer.lib.models.Album;
import maestro.lastfm.lib.BaseRequestHelper;
import maestro.lastfm.lib.LastFmAlbum;
import maestro.lastfm.lib.LastFmApi;
import maestro.lastfm.lib.LastFmImage;
import maestro.mymusic.Constants;
import maestro.mymusic.R;
import maestro.mymusic.utils.AlbumCoverMaker;
import maestro.mymusic.utils.adapters.BaseUpdateAdapter;

/**
 * Created by artyom on 8/28/14.
 */
public class AlbumCoverFetchDialog extends BaseFragment implements BaseRequestHelper.RequestListener {

    public static final String TAG = AlbumCoverMaker.class.getSimpleName();

    private static final String PARAM_ALBUM = "param_album";
    private static final String SAVED_ITEMS = "saved_items";

    public static final AlbumCoverFetchDialog makeInstance(Album album) {
        AlbumCoverFetchDialog dialog = new AlbumCoverFetchDialog();
        Bundle args = new Bundle(1);
        args.putParcelable(PARAM_ALBUM, album);
        dialog.setArguments(args);
        return dialog;
    }

    private GridView mGrid;
    private ProgressBar mProgress;
    private TextView mTextView;
    private CoverFetchAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.album_cover_fetch_dialog_view, null);
        mGrid = (GridView) v.findViewById(R.id.list);
        mProgress = (ProgressBar) v.findViewById(R.id.progress);
        mTextView = (TextView) v.findViewById(R.id.text);
        mGrid.setNumColumns(isTablet ? 4 : 2);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mGrid.setAdapter(mAdapter = new CoverFetchAdapter(getActivity()));

        final ViewTreeObserver observer = mGrid.getViewTreeObserver();
        if (observer != null) {
            observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    mAdapter.setSize(mGrid.getWidth() / (isTablet ? 4 : 2));
                    mGrid.getViewTreeObserver().removeOnPreDrawListener(this);
                    return false;
                }
            });
        }

        Album album = getArguments().getParcelable(PARAM_ALBUM);
        if (savedInstanceState != null
                && savedInstanceState.containsKey(SAVED_ITEMS + album.Title)) {
            mProgress.setVisibility(View.GONE);
            mAdapter.update((LastFmAlbum[]) savedInstanceState.getParcelableArray(SAVED_ITEMS + album.Title));
        } else {
            LastFmApi.findAlbum(LastFmApi.buildAlbumSearchRequest(album.Title), this, true);
        }
    }

    @Override
    public void onResult(final Object result) {
        if (getActivity() != null)
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mProgress.setVisibility(View.GONE);
                    if (result instanceof LastFmAlbum[]) {
                        if (mAdapter != null)
                            mAdapter.update((LastFmAlbum[]) result);
                    } else {
                        BaseRequestHelper.BASE_NETWORK_ERROR error = (BaseRequestHelper.BASE_NETWORK_ERROR) result;
                        switch (error) {
                            case NO_RESUTL:
                                mTextView.setText(R.string.no_search_results);
                                break;
                            default:
                                mTextView.setText(error.name());
                        }
                    }
                }
            });
    }

    @Override
    public boolean canDoRequest() {
        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Album album = getArguments().getParcelable(PARAM_ALBUM);
        if (mAdapter != null && mAdapter.getItems() != null) {
            outState.putParcelableArray(SAVED_ITEMS + album.Title, mAdapter.getItems());
        }
    }

    private final class CoverFetchAdapter extends BaseUpdateAdapter<LastFmAlbum> {

        private MIM2 mim;
        private int size;

        public CoverFetchAdapter(Context context) {
            super(context);
            mim = MIMManager.getInstance().getMIM(Constants.NETWORK_MIM);
            if (mim == null) {
                mim = new MIM2(context.getApplicationContext()).setMaker(new MIMInternetMaker(false));
                MIMManager.getInstance().addMIM(Constants.NETWORK_MIM, mim);
            }
        }

        public void setSize(int size) {
            this.size = size;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        public View getView(int position, View v, ViewGroup viewGroup) {
            Holder h;
            if (v == null) {
                v = mInflater.inflate(R.layout.album_cover_fetch_item_view, null);
                h = new Holder();
                h.Image = (ImageView) v.findViewById(R.id.image);
                h.Image.setLayoutParams(new RelativeLayout.LayoutParams(size, size));
                v.setTag(h);
            } else {
                h = (Holder) v.getTag();
            }
            LastFmAlbum album = getItem(position);
            LastFmImage image = album.getImage(LastFmApi.IMAGE_EXTRA_LARGE);
            MIM2.ImageLoadObject loadObject = new MIM2.ImageLoadObject(image.getUrl(),
                    image.getUrl()).setWidth(size).setHeight(size);
            mim.loadImage(loadObject, h.Image);
            return v;
        }

        class Holder {
            ImageView Image;
        }

    }

}
