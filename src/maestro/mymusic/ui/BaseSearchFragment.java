package maestro.mymusic.ui;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.msoft.android.mplayer.lib.models.BaseMediaItem;
import maestro.mymusic.R;
import maestro.mymusic.utils.Loaders;
import maestro.mymusic.utils.adapters.UniqRowAdapter;

/**
 * Created by artyom on 9/10/14.
 */
public class BaseSearchFragment extends BaseFragment implements LoaderManager.LoaderCallbacks<BaseMediaItem[]> {

    public static final String TAG = BaseSearchFragment.class.getSimpleName();

    private static final String PARAM_QUERY = "param_query";
    private static final int loaderId = TAG.hashCode();

    private ListView mList;
    private TextView mNoResultText;
    private ProgressBar mProgress;
    private EditText mEditText;
    private UniqRowAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.search_fragment_view, null);
        mList = (ListView) v.findViewById(R.id.list);
        mNoResultText = (TextView) v.findViewById(R.id.no_result_text);
        mEditText = (EditText) v.findViewById(R.id.edit_text);
        mProgress = (ProgressBar) v.findViewById(R.id.progress);
        mEditText.addTextChangedListener(mEditWatcher);
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().getActionBar().hide();
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().getActionBar().show();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mList.setAdapter(mAdapter = new UniqRowAdapter(getActivity()).setIsForPlayView(true));
    }

    @Override
    public Loader<BaseMediaItem[]> onCreateLoader(int i, Bundle bundle) {
        animateProgress(true);
        animateNoResultsView(false);
        animateListView(false);
        return new Loaders.GlobalSearchLoader(getActivity(), bundle.getString(PARAM_QUERY));
    }

    @Override
    public void onLoadFinished(Loader<BaseMediaItem[]> loader, BaseMediaItem[] baseMediaItems) {
        if (mAdapter != null) {
            mAdapter.update(baseMediaItems);
            animateProgress(false);
            if (mAdapter.getCount() == 0) {
                animateNoResultsView(true);
            } else {
                animateListView(true);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<BaseMediaItem[]> loader) {

    }

    void animateProgress(boolean visible) {
        if (visible) {
            mProgress.animate().alpha(1f).start();
        } else {
            mProgress.animate().alpha(0f).start();
        }
    }

    void animateNoResultsView(boolean visible) {
        if (visible) {
            mNoResultText.animate().alpha(1f).start();
        } else {
            mNoResultText.animate().alpha(0f).start();
        }
    }

    void animateListView(boolean visible) {
        if (visible) {
            mList.animate().alpha(1f).start();
        } else {
            mList.animate().alpha(0f).start();
        }
    }

    public void search(String query) {
        if (mAdapter != null) {
            if (query != null && !TextUtils.isEmpty(query)) {
                Bundle bundle = new Bundle(1);
                bundle.putString(PARAM_QUERY, query);
                if (getLoaderManager().getLoader(loaderId) != null) {
                    getLoaderManager().restartLoader(loaderId, bundle, this);
                } else {
                    getLoaderManager().initLoader(loaderId, bundle, this);
                }
            } else {
                mAdapter.update(null);
            }
        }
    }

    private TextWatcher mEditWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            search(editable.toString());
        }
    };


}
