package maestro.mymusic.ui;

import android.os.Bundle;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.AdapterView;
import com.msoft.android.mplayer.lib.models.Artist;
import com.msoft.android.mplayer.lib.models.BaseMediaItem;
import maestro.mymusic.Main;
import maestro.mymusic.utils.Loaders;
import maestro.mymusic.utils.adapters.SimpleArtistAdapter;

/**
 * Created by artyom on 7/11/14.
 */
public class ArtistsFragment extends BaseListFragment<Artist> {

    private SimpleArtistAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionbarToggleEnable(!isTablet);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setAdapter(mAdapter = new SimpleArtistAdapter(getActivity(), MainActivity.getMainListWidth()));
        setOnScrollListener(mAdapter);
        load();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (MainActivity != null)
            MainActivity.openMusicPreviewFragment((BaseMediaItem) adapterView.getItemAtPosition(i));
    }

    @Override
    public int getLoaderId() {
        return Main.MENU_ITEM_ARTISTS;
    }

    @Override
    public Loader<Artist[]> onCreateLoader(int i, Bundle bundle) {
        return new Loaders.ArtistLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<Artist[]> tLoader, Artist[] t) {
        super.onLoadFinished(tLoader, t);
        mAdapter.update(t);
    }
}
