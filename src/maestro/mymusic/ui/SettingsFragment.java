package maestro.mymusic.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.dream.android.mim.RoundedColorDrawable;
import maestro.mymusic.R;
import maestro.mymusic.utils.SettingsHolder;

/**
 * Created by artyom on 9/20/14.
 */
public class SettingsFragment extends Fragment {

    public static final String TAG = SettingsFragment.class.getSimpleName();

    private LinearLayout mParent;
    private int padding10dp;
    private int heaerTextSize;
    private int simpleTextSize;
    private int circleItemSize;
    private int screenWidth;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final DisplayMetrics metrics = getResources().getDisplayMetrics();
        padding10dp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, metrics);
        heaerTextSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 18, metrics);
        simpleTextSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 14, metrics);
        circleItemSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60, metrics);
        screenWidth = metrics.widthPixels;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mParent = new LinearLayout(getActivity());
        mParent.setOrientation(LinearLayout.VERTICAL);
        mParent.setBackgroundColor(Color.WHITE);
        mParent.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        mParent.setPadding(padding10dp, 0, padding10dp, 0);
        return mParent;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mParent.addView(getHeaderView(new SettingItem(getString(R.string.primary_color))));
        SettingItem[] colorItems = new SettingItem[SettingsHolder.COLORS.length];
        for (int i = 0; i < colorItems.length; i++) {
            colorItems[i] = new SettingItem(SettingsHolder.COLORS[i]);
        }
        mParent.addView(getCircleSelectView(colorItems));
    }

    private View getCircleSelectView(SettingItem[] items) {
        final FrameLayout parent = new FrameLayout(getActivity());
        final int itemsPerRow = (screenWidth - padding10dp) / (circleItemSize + padding10dp);
        final int count = items.length;
        int rowsCount = count / itemsPerRow;
        if (rowsCount * itemsPerRow != count) {
            rowsCount++;
        }
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SettingItem item = (SettingItem) view.getTag();
                SettingsHolder.getInstance().setPrimaryColor(item.Color);
            }
        };
        for (int i = 0; i < rowsCount; i++) {
            for (int j = 0; j < itemsPerRow; j++) {
                final int realItem = j + (i * itemsPerRow);
                if (realItem > count - 1)
                    break;
                SettingItem item = items[realItem];
                FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(circleItemSize, circleItemSize);
                params.leftMargin = (circleItemSize + padding10dp) * j;
                params.topMargin = (circleItemSize + padding10dp) * i;
                parent.addView(getCircleItemView(item, listener), params);
            }
        }
        return parent;
    }

    private View getCircleItemView(final SettingItem item, View.OnClickListener listener) {
        View view = new View(getActivity());
        view.setBackgroundDrawable(new RoundedColorDrawable(item.Color, circleItemSize / 2));
        view.setTag(item);
        view.setOnClickListener(listener);
        return view;
    }

    private View getHeaderView(SettingItem item) {
        TextView textView = new TextView(getActivity());
        textView.setText(item.Title);
        textView.setPadding(padding10dp, padding10dp, padding10dp, padding10dp);
        textView.setTextSize(heaerTextSize);
        return textView;
    }

    private class SettingItem {

        public String Title;
        public String Key;
        public int Color;

        public SettingItem(int color) {
            this.Color = color;
        }

        public SettingItem(String title) {
            this.Title = title;
        }

        public SettingItem(String title, String key) {
            this(title);
            this.Key = key;
        }

        public SettingItem(String title, String key, int color) {
            this(title, key);
            this.Color = color;
        }

    }

}