package maestro.mymusic;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.util.TypedValue;
import android.view.*;
import android.widget.*;
import com.dream.android.mim.MIM2;
import com.dream.android.mim.MIMCirclePostMaker;
import com.dream.android.mim.MIMManager;
import com.dream.android.mim.MIMResourceMaker;
import com.msoft.android.mplayer.lib.models.BaseMediaItem;
import com.msoft.android.mplayer.lib.models.Song;
import com.msoft.android.service.MusicPlayerEngine;
import maestro.mymusic.ui.*;
import maestro.mymusic.utils.AlbumCoverMaker;
import maestro.mymusic.utils.MFlipDisplayer;
import maestro.mymusic.utils.Typefacer;
import maestro.mymusic.views.SlidingMenu;

import java.util.ArrayList;

public class Main extends FragmentActivity implements AdapterView.OnItemClickListener, View.OnClickListener,
        MusicPlayerEngine.MusicPlayerListener {

    public static final String TAG = Main.class.getSimpleName();

    private DrawerLayout mDrawer;
    private ListView mNavigationList;
    private ImageButton mDrawerControlButton;
    private RelativeLayout mControlLayout, mPlayControlLayout;

    public static final int MENU_ITEM_ALBUMS = 0;
    public static final int MENU_ITEM_ALL_SONGS = 1;
    public static final int MENU_ITEM_PLAYLIST = 2;
    public static final int MENU_ITEM_FAVOURITE = 3;
    public static final int MENU_ITEM_ARTISTS = 4;
    public static final int MENU_ITEM_GENRES = 5;

    public static final int MENU_ITEM_SETTINGS = 6;

    private ArrayList<NavigationItem> mNavigationItems = new ArrayList<NavigationItem>();
    private ArrayList<NavigationItem> mUserNavigationItems = new ArrayList<NavigationItem>();
    private NavigationAdapter mNavigationAdapter;
    private NavigationItem mCurrentSelectedNavigationItem;
    private FragmentTransaction mTransaction;
    private BaseSearchFragment mSearchFragment;
    private SlidingMenu slidingMenu;

    private ImageButton btnPrev, btnPlay, btnNext;

    private boolean isControlVisible = true;

    private boolean isTablet;
    private boolean is7inchTablet;
    private boolean is10inchTablet;
    private boolean isLandscape;
    private CONTROL_STATE mControlState;

    private Drawable mActionBarDrawable;

    private MIM2 mim = MIMManager.getInstance().getMIM(Constants.SONG_COVER_MIM);
    private MIMCirclePostMaker mCirclePostMaker;

    private enum CONTROL_STATE {
        FULL, DRAWER_ONLY, LOCKED
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        View decorView = getWindow().getDecorView();
        if (decorView != null) {
            slidingMenu = new SlidingMenu(getApplicationContext());
            slidingMenu.setContentView(getUserMenuView());
            ((FrameLayout) decorView).addView(slidingMenu,
                    new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        }
        setContentView(R.layout.main);

        final int titleId = getResources().getIdentifier("action_bar_title", "id", "android");
        final TextView abTextView = (TextView) findViewById(titleId);
        abTextView.setTypeface(Typefacer.Lobster);
        abTextView.setShadowLayer(3f, -3, 3, Color.BLACK);
        getActionBar().setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));

        is7inchTablet = getResources().getBoolean(R.bool.is7inch);
        is10inchTablet = getResources().getBoolean(R.bool.is10inch);
        isTablet = is10inchTablet || is7inchTablet;
        isLandscape = getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;

        mControlState = !isTablet || (is7inchTablet && !isLandscape)
                ? CONTROL_STATE.FULL : (is7inchTablet && isLandscape)
                ? CONTROL_STATE.DRAWER_ONLY : CONTROL_STATE.LOCKED;

        if (mim == null) {
            mim = new MIM2(getApplicationContext()).setMaker(new AlbumCoverMaker(getApplicationContext()));
            MIMManager.getInstance().addMIM(Constants.SONG_COVER_MIM, mim);
        }
        mCirclePostMaker = new MIMCirclePostMaker((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                4, getResources().getDisplayMetrics()), "#ffffff");

        mActionBarDrawable = new ColorDrawable(getResources().getColor(R.color.actb_color));
        getActionBar().setBackgroundDrawable(mActionBarDrawable);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        mNavigationList = (ListView) findViewById(R.id.navigation_list);
        mNavigationList.setOnItemClickListener(this);

        mControlLayout = (RelativeLayout) findViewById(R.id.control_layout);
        mPlayControlLayout = (RelativeLayout) findViewById(R.id.play_control_main_layout);
        mPlayControlLayout.setOnTouchListener(mPlayControlButtonTouchListener);
        mPlayControlLayout.setOnClickListener(this);

        btnPrev = (ImageButton) findViewById(R.id.prev_button);
        btnPlay = (ImageButton) findViewById(R.id.play_button);
        btnNext = (ImageButton) findViewById(R.id.next_button);

        btnPrev.setOnClickListener(this);
        btnPlay.setOnClickListener(this);
        btnNext.setOnClickListener(this);

        prepareNavigationList();
        if (mDrawer != null) {
            prepareDrawer();
        }

        if (savedInstanceState == null) {
            openFragmentById(MENU_ITEM_ALBUMS, true);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.search) {
            openSearchFragment();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MusicPlayerEngine.getInstance().detachListener(TAG);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MusicPlayerEngine.getInstance().attachListener(TAG, this);
        if (MusicPlayerEngine.getInstance().canPlay()) {
            updatePlaying();
        } else {
            mPlayControlLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        if (slidingMenu.isOpened()) {
            slidingMenu.closeMenu();
        } else
            super.onBackPressed();
    }

    void openSearchFragment() {
        mSearchFragment = (BaseSearchFragment) getSupportFragmentManager().findFragmentByTag(BaseSearchFragment.TAG);
        if (mSearchFragment == null) {
            mSearchFragment = new BaseSearchFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.search_frame, mSearchFragment, BaseSearchFragment.TAG)
                    .addToBackStack(BaseSearchFragment.TAG).commit();
        }
    }

    void hideSearchFragment() {
        if (mSearchFragment != null)
            getSupportFragmentManager().beginTransaction().remove(mSearchFragment);
    }

    private void prepareNavigationList() {
        mNavigationItems.add(new NavigationItem(MENU_ITEM_ALBUMS, R.drawable.albums_selector));
        mNavigationItems.add(new NavigationItem(MENU_ITEM_ALL_SONGS, R.drawable.music_selector));
        mNavigationItems.add(new NavigationItem(MENU_ITEM_PLAYLIST, R.drawable.playlist_selector));
        mNavigationItems.add(new NavigationItem(MENU_ITEM_FAVOURITE, R.drawable.fav_music_selector));
        mNavigationItems.add(new NavigationItem(MENU_ITEM_ARTISTS, R.drawable.singers_selector));
        mNavigationItems.add(new NavigationItem(MENU_ITEM_GENRES, R.drawable.genres_selector));
        if (mCurrentSelectedNavigationItem == null) {
            mCurrentSelectedNavigationItem = mNavigationItems.get(0);
        }
        mNavigationList.setAdapter(mNavigationAdapter = new NavigationAdapter());

        mUserNavigationItems.add(new NavigationItem(getString(R.string.settings), MENU_ITEM_SETTINGS, -1));
    }

    private void prepareDrawer() {
        mDrawerControlButton = (ImageButton) findViewById(R.id.drawer_control_button);
        findViewById(R.id.drawer_left).getLayoutParams().width = getResources().getDisplayMetrics().widthPixels;
        mDrawerControlButton.setOnClickListener(this);
        mDrawer.setScrimColor(Color.TRANSPARENT);
        mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, Gravity.LEFT);
        mDrawer.setDrawerListener(new DrawerLayout.DrawerListener() {

            @Override
            public void onDrawerSlide(View view, float v) {
                if (view.equals(findViewById(R.id.drawer_right))) {
                    final float x = view.getX()
                            - findViewById(R.id.main_content_layout).getWidth();
                    findViewById(R.id.main_content_layout).setX(x);
                    findViewById(getResources().getIdentifier("action_bar_container", "id", "android")).setX(x);
                } else if (!isTablet && view.equals(findViewById(R.id.drawer_left))) {
                    mActionBarDrawable.setAlpha((int) (255 * (1f - v)));
                    getActionBar().setBackgroundDrawable(mActionBarDrawable);
                }
            }

            @Override
            public void onDrawerOpened(View view) {
                if (!isControlVisible && !getActionBar().isShowing())
                    toggleControl(true, isControlVisible);
                if (view.equals(findViewById(R.id.drawer_left))) {
                    mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, view);
                }
                commitTransaction();
            }

            @Override
            public void onDrawerClosed(View view) {
                if (!isControlVisible)
                    toggleControl(false);
                if (view.equals(findViewById(R.id.drawer_left))) {
                    mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, view);
                    getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager()
                            .findFragmentByTag(MusicPreviewFragment.TAG)).commit();
                }
                commitTransaction();
            }

            @Override
            public void onDrawerStateChanged(int i) {

            }
        });

    }

    @Override
    public void onClick(View view) {
        final int id = view.getId();
        switch (id) {
            case R.id.drawer_control_button:
                if (mDrawer.isDrawerOpen(Gravity.RIGHT)) {
                    mDrawer.closeDrawers();
                } else {
                    mDrawer.openDrawer(Gravity.RIGHT);
                }
                break;
            case R.id.play_control_main_layout:
                openMusicPlayFragment();
                break;
            case R.id.play_button:
                if (MusicPlayerEngine.getInstance().isPlaying())
                    MusicPlayerEngine.getInstance().pause();
                else MusicPlayerEngine.getInstance().resume();
                break;
            case R.id.next_button:
                MusicPlayerEngine.getInstance().next();
                break;
            case R.id.prev_button:
                MusicPlayerEngine.getInstance().prev();
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (l == MENU_ITEM_SETTINGS) {
            openSettingsFragment();
        } else {
            openFragmentById((int) l, false);
        }
        if (mDrawer != null)
            mDrawer.closeDrawer(Gravity.RIGHT);
        if (slidingMenu.isOpened())
            slidingMenu.closeMenu();
    }

    @Override
    public void onMusicPlayerEvent(MusicPlayerEngine.MUSIC_PLAYER_EVENT event) {
        switch (event) {
            case TIME_UPDATE:
                break;
            default:
                updatePlaying();
        }
    }

    public void openFragmentById(int id, boolean commit) {
        if (mTransaction == null) {
            mTransaction = getSupportFragmentManager().beginTransaction();
        }
        switch (id) {
            case MENU_ITEM_ALBUMS:
                mTransaction.replace(R.id.content_frame, new AlbumFragment());
                break;
            case MENU_ITEM_ALL_SONGS:
                mTransaction.replace(R.id.content_frame, new SongsFragment());
                break;
            case MENU_ITEM_PLAYLIST:
                mTransaction.replace(R.id.content_frame, new PlaylistFragment());
                break;
            case MENU_ITEM_ARTISTS:
                mTransaction.replace(R.id.content_frame, new ArtistsFragment());
                break;
            case MENU_ITEM_GENRES:
                mTransaction.replace(R.id.content_frame, new GenresFragment());
                break;
        }
        if (commit || mDrawer == null)
            commitTransaction();
    }

    private void commitTransaction() {
        if (mTransaction != null) {
            mTransaction.commit();
            mTransaction = null;
        }
    }

    public void openMusicPreviewFragment(BaseMediaItem item) {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(MusicPreviewFragment.TAG);
        if (fragment == null) {
            fragment = new MusicPreviewFragment();
            if (mTransaction == null)
                mTransaction = getSupportFragmentManager().beginTransaction();
            mTransaction.add(R.id.drawer_left, fragment, MusicPreviewFragment.TAG);
        }
        ((MusicPreviewFragment) fragment).resetItem(item);
        if (mDrawer != null && !(is7inchTablet && isLandscape) && !is10inchTablet)
            mDrawer.openDrawer(Gravity.LEFT);
        else
            commitTransaction();
    }

    public void openMusicPlayFragment() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(PlayFragment.TAG);
        if (fragment == null) {
            fragment = new PlayFragment();
            if (mTransaction == null)
                mTransaction = getSupportFragmentManager().beginTransaction();
            mTransaction.setCustomAnimations(R.anim.translate_up, R.anim.translate_down, R.anim.translate_up, R.anim.translate_down)
                    .add(R.id.main_content_layout, fragment, PlayFragment.TAG).addToBackStack(null);
            if (mDrawer != null && !(is7inchTablet && isLandscape) && !is10inchTablet && mDrawer.isDrawerOpen(Gravity.LEFT))
                mDrawer.closeDrawer(Gravity.LEFT);
            else
                commitTransaction();
        }
    }

    public void openSettingsFragment() {
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.anim.translate_up, R.anim.translate_down, R.anim.translate_up, R.anim.translate_down)
                .replace(R.id.main_content_layout, new SettingsFragment())
                .addToBackStack(null).commit();
    }

    public void toggleControl(boolean show) {
        toggleControl(show, show);
    }

    public void toggleControl(boolean show, boolean set) {
        if (mControlState != CONTROL_STATE.LOCKED) {
            if (!show && isControlVisible) {
                if (mControlState != CONTROL_STATE.DRAWER_ONLY)
                    getActionBar().hide();
                ObjectAnimator tAnim = ObjectAnimator.ofFloat(mControlLayout, "translationY", 0f,
                        mDrawerControlButton.getHeight() + ((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30,
                                getResources().getDisplayMetrics())));
                tAnim.start();
                isControlVisible = set;
            } else if (show && !isControlVisible) {
                if (mControlState != CONTROL_STATE.DRAWER_ONLY)
                    getActionBar().show();
                ObjectAnimator tAnim = ObjectAnimator.ofFloat(mControlLayout, "translationY",
                        mDrawerControlButton.getHeight() + ((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30,
                                getResources().getDisplayMetrics())), 0f);
                tAnim.start();
                isControlVisible = set;
            }
        }
    }

    public void setActionBarAlpha(int alpha) {
        if (mActionBarDrawable != null)
            mActionBarDrawable.setAlpha(alpha);
    }

    public int getMainListWidth() {
        return !isTablet || (is7inchTablet && !isLandscape)
                ? getResources().getDisplayMetrics().widthPixels
                : is7inchTablet ? (int) (getResources().getDisplayMetrics().widthPixels / 2 * 0.9f)
                : 0;
    }

    public int getSecondListWidth() {
        return !isTablet || (is7inchTablet && !isLandscape)
                ? getResources().getDisplayMetrics().widthPixels
                : is7inchTablet ? (int) (getResources().getDisplayMetrics().widthPixels / 2 * 0.9f)
                : 0;
    }

    private void updatePlaying() {
        Song currentSong = MusicPlayerEngine.getInstance().getCurrentPlayingSong();
        if (currentSong == null) return;
        if (mPlayControlLayout.getVisibility() == View.GONE) {
            mPlayControlLayout.setVisibility(View.VISIBLE);
            ObjectAnimator tAnim = ObjectAnimator.ofFloat(mPlayControlLayout, "translationY",
                    mDrawerControlButton.getHeight() + ((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30,
                            getResources().getDisplayMetrics())), 0f);
            tAnim.start();
        }
        String key = currentSong.Title + "_song_" + currentSong.Duration + "_main";
        final int size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                80, getResources().getDisplayMetrics());
        MIM2.ImageLoadObject loadObject = new MIM2.ImageLoadObject(key, key)
                .setDisplayer(new MFlipDisplayer())
                .setPostMaker(mCirclePostMaker).setWidth(size).setHeight(size)
                .setObj(currentSong).setUseDisplayer(true);
        mim.loadImage(loadObject, (ImageView) findViewById(R.id.main_image));
        updatePlayingControl();
    }

    private void updatePlayingControl() {
        if (MusicPlayerEngine.getInstance().isPlaying()) {
            btnPlay.setImageResource(R.drawable.ic_pause);
        } else {
            btnPlay.setImageResource(R.drawable.ic_play);
        }
    }

    public CONTROL_STATE getControlState() {
        return mControlState;
    }

    final View getUserMenuView() {
        ListView userList = (ListView) getLayoutInflater().inflate(R.layout.user_menu_view, null);
        View userHeader = getLayoutInflater().inflate(R.layout.user_header_view, null);
        userList.addHeaderView(userHeader);
        userList.setOnItemClickListener(this);
        userList.setAdapter(new UserNavigationAdapter());
        return userList;
    }

    private final class NavigationItem {
        public int Id;
        public int ResId;
        public String Title;

        public NavigationItem(int id, int resId) {
            Id = id;
            ResId = resId;
        }

        public NavigationItem(String title, int id, int resId) {
            this(id, resId);
            Title = title;
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof NavigationItem) {
                NavigationItem _o = (NavigationItem) o;
                return _o.Id == Id && _o.ResId == ResId;
            }
            return super.equals(o);
        }
    }

    private final class NavigationAdapter extends BaseAdapter {

        private LayoutInflater mInflater;
        private MIM2 mim = MIMManager.getInstance().getMIM(Constants.NAVIGATION_MIM);

        public NavigationAdapter() {
            mInflater = getLayoutInflater();
            if (mim == null) {
                final int defSize = getResources().getDimensionPixelSize(R.dimen.base_navigation_item_size);
                mim = new MIM2(getApplicationContext())
                        .setMaker(new MIMResourceMaker(getApplicationContext()))
                        .setDefaultSize(defSize, defSize);
            }
        }

        @Override
        public int getCount() {
            return mNavigationItems.size();
        }

        @Override
        public Object getItem(int i) {
            return mNavigationItems.get(i);
        }

        @Override
        public long getItemId(int i) {
            return mNavigationItems.get(i).Id;
        }

        @Override
        public View getView(int position, View v, ViewGroup viewGroup) {
            Holder h;
            if (v == null) {
                h = new Holder();
                v = mInflater.inflate(R.layout.navigation_list_item_view, null);
                h.Image = (ImageView) v.findViewById(R.id.navigation_list_item_view_image);
                v.setTag(h);
            } else {
                h = (Holder) v.getTag();
            }
            NavigationItem localItem = mNavigationItems.get(position);
            h.Image.setImageResource(localItem.ResId);
            v.setActivated(mCurrentSelectedNavigationItem.equals(localItem));
            return v;
        }

        class Holder {
            ImageView Image;
        }

    }

    private final class UserNavigationAdapter extends BaseAdapter {

        private LayoutInflater mInflater;
        private MIM2 mim = MIMManager.getInstance().getMIM(Constants.NAVIGATION_MIM);

        public UserNavigationAdapter() {
            mInflater = getLayoutInflater();
            if (mim == null) {
                final int defSize = getResources().getDimensionPixelSize(R.dimen.base_navigation_item_size);
                mim = new MIM2(getApplicationContext())
                        .setMaker(new MIMResourceMaker(getApplicationContext()))
                        .setDefaultSize(defSize, defSize);
            }
        }

        @Override
        public int getCount() {
            return mUserNavigationItems.size();
        }

        @Override
        public NavigationItem getItem(int i) {
            return mUserNavigationItems.get(i);
        }

        @Override
        public long getItemId(int i) {
            return mUserNavigationItems.get(i).Id;
        }

        @Override
        public View getView(int position, View v, ViewGroup viewGroup) {
            Holder h;
            if (v == null) {
                h = new Holder();
                v = mInflater.inflate(R.layout.user_menu_item, null);
                h.Image = (ImageView) v.findViewById(R.id.image);
                h.Title = (TextView) v.findViewById(R.id.title);
            } else {
                h = (Holder) v.getTag();
            }
            NavigationItem item = getItem(position);
            h.Title.setText(item.Title);
            h.Image.setImageResource(item.ResId);
            return v;
        }

        class Holder {
            ImageView Image;
            TextView Title;
        }

    }

    private final View.OnTouchListener mPlayControlButtonTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            final float x = motionEvent.getX();
            final float y = motionEvent.getY();

            final float rowX = motionEvent.getRawX();
            final float rowY = motionEvent.getRawY();

            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    uiHandler.sendEmptyMessageDelayed(MSG_SHOW_CONTROL_BUTTONS, DEF_MSG_DELAY);
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (matchView(btnPrev, rowX, rowY)) {
                        btnNext.setPressed(false);
                        btnPlay.setPressed(false);
                        if (!btnPrev.isPressed())
                            btnPrev.setPressed(true);
                    } else if (matchView(btnPlay, rowX, rowY)) {
                        btnNext.setPressed(false);
                        btnPrev.setPressed(false);
                        if (!btnPlay.isPressed())
                            btnPlay.setPressed(true);
                    } else if (matchView(btnNext, rowX, rowY)) {
                        btnPrev.setPressed(false);
                        btnPlay.setPressed(false);
                        if (!btnNext.isPressed())
                            btnNext.setPressed(true);
                    } else {
                        btnPrev.setPressed(false);
                        btnNext.setPressed(false);
                        btnPlay.setPressed(false);
                    }
                    break;
                case MotionEvent.ACTION_CANCEL:
                case MotionEvent.ACTION_UP:
                    uiHandler.removeMessages(MSG_SHOW_CONTROL_BUTTONS);
                    if (x > 0 && y > 0) {
                        view.performClick();
                    } else if (matchView(btnPrev, rowX, rowY)) {
                        btnPrev.performClick();
                    } else if (matchView(btnPlay, rowX, rowY)) {
                        btnPlay.performClick();
                    } else if (matchView(btnNext, rowX, rowY)) {
                        btnNext.performClick();
                    }
                    hidePlayControlButtons();
                    break;
            }

            return true;
        }

        private boolean matchView(View view, float rowX, float rowY) {
            return rowX >= view.getLeft() && rowX <= view.getRight()
                    && rowY >= view.getTop() && rowY <= view.getBottom();
        }

    };

    private static final long DEF_MSG_DELAY = 200;
    private static final int MSG_SHOW_CONTROL_BUTTONS = 0;

    private Handler uiHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_SHOW_CONTROL_BUTTONS:
                    showPlayControlButtons();
                    break;
            }
        }
    };

    private void showPlayControlButtons() {
        AnimatorSet set = new AnimatorSet();
        ObjectAnimator aAnim = ObjectAnimator.ofFloat(btnPrev, "alpha", 0, 1f);
        ObjectAnimator aAnim2 = ObjectAnimator.ofFloat(btnPlay, "alpha", 0, 1f);
        ObjectAnimator aAnim3 = ObjectAnimator.ofFloat(btnNext, "alpha", 0, 1f);
        set.setDuration(150).playTogether(aAnim, aAnim2, aAnim3);
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                btnPrev.setVisibility(View.VISIBLE);
                btnNext.setVisibility(View.VISIBLE);
                btnPlay.setVisibility(View.VISIBLE);
            }
        });
        set.start();
    }

    private void hidePlayControlButtons() {
        AnimatorSet set = new AnimatorSet();
        ObjectAnimator aAnim = ObjectAnimator.ofFloat(btnPrev, "alpha", 1f, 0);
        ObjectAnimator aAnim2 = ObjectAnimator.ofFloat(btnPlay, "alpha", 1f, 0);
        ObjectAnimator aAnim3 = ObjectAnimator.ofFloat(btnNext, "alpha", 1f, 0);
        set.setDuration(150).playTogether(aAnim, aAnim2, aAnim3);
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                btnPrev.setVisibility(View.GONE);
                btnNext.setVisibility(View.GONE);
                btnPlay.setVisibility(View.GONE);
            }
        });
        set.start();
    }

    public Bitmap getDrawingCache() {
        return mDrawer != null ? mDrawer.getDrawingCache() : null;
    }

}
