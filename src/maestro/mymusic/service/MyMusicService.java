package maestro.mymusic.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.util.TypedValue;
import android.widget.Button;
import android.widget.RemoteViews;
import com.dream.android.mim.MIM2;
import com.dream.android.mim.MIMManager;
import com.msoft.android.mplayer.lib.models.Song;
import com.msoft.android.service.MusicPlayerEngine;
import com.msoft.android.service.MusicPlayerService;
import maestro.mymusic.Constants;
import maestro.mymusic.R;
import maestro.mymusic.utils.AlbumCoverMaker;

import java.util.HashMap;

/**
 * Created by artyom on 8/20/14.
 */
public class MyMusicService extends Service implements MusicPlayerEngine.MusicPlayerListener {

    public static final String TAG = MyMusicService.class.getSimpleName();

    private MIM2 mim;
    private Song mCurrentSong;

    private NotificationManager mNotificationManager;

    private final LocalBinder mBinder = new LocalBinder();
    private MusicPlayerEngine mEngine = MusicPlayerEngine.getInstance();

    private final int NOTIFICATION_ID = TAG.hashCode();
    private static final String NOTIFICATION_NEXT = MusicPlayerEngine.PACKAGE + ".NOTIFICATION_NEXT";
    private static final String NOTIFICATION_PLAY = MusicPlayerEngine.PACKAGE + ".NOTIFICATION_PLAY";

    private IntentFilter mNotificationBroadcastFilter = new IntentFilter();

    {
        mNotificationBroadcastFilter.addAction(NOTIFICATION_NEXT);
        mNotificationBroadcastFilter.addAction(NOTIFICATION_PLAY);
    }

    private BroadcastReceiver mNotificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(NOTIFICATION_NEXT)) {
                mEngine.next();
            } else if (intent.getAction().equals(NOTIFICATION_PLAY)) {
                if (mEngine.isPlaying()) {
                    mEngine.pause();
                } else {
                    mEngine.resume();
                }
            }
        }
    };

    public class LocalBinder extends Binder {
        public MyMusicService getService() {
            return MyMusicService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mim = MIMManager.getInstance().getMIM(Constants.SONG_COVER_MIM);
        if (mim == null) {
            final int size = getResources().getDimensionPixelSize(R.dimen.base_cover_size);
            mim = new MIM2(getApplicationContext()).setDefaultSize(size, size).setMaker(new AlbumCoverMaker(this));
        }
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        MusicPlayerEngine.getInstance().attachListener(TAG, this);
        registerReceiver(mNotificationReceiver, mNotificationBroadcastFilter);
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mNotificationReceiver);
        MusicPlayerEngine.getInstance().detachListener(TAG);
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onMusicPlayerEvent(MusicPlayerEngine.MUSIC_PLAYER_EVENT event) {
        switch (event) {
            case START:
            case PAUSE:
            case RESUME:
                updateNotification();
                break;
            case ERROR:
            case END:
                removeNotification();
                break;
        }
    }

    private void updateNotification() {
        final int size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 64, getResources().getDisplayMetrics());
        mCurrentSong = MusicPlayerEngine.getInstance().getCurrentPlayingSong();
        if(mCurrentSong == null) return;
        String key = mCurrentSong.Title + "_song_" + mCurrentSong.Duration + "_notification";
        MIM2.ImageLoadObject loadObject = new MIM2.ImageLoadObject(key, key).setObj(mCurrentSong)
                .setWidth(size).setHeight(size)
                .setListener(new MIM2.OnImageLoadEventListener() {
                    @Override
                    public void onImageLoadEvent(IMAGE_LOAD_EVENT event, Object obj) {
                        if (obj instanceof MIM2.ImageLoadObject) {
                            MIM2.ImageLoadObject localLoadObject = (MIM2.ImageLoadObject) obj;
                            updateNotification((Song) localLoadObject.getObj(), localLoadObject.getResultBitmap());
                        }
                    }
                });
        mim.buildTask(loadObject, null).asyncWithCacheCheck();
    }

    private void updateNotification(Song song, Bitmap bitmap) {
        RemoteViews remoteView = new RemoteViews(getPackageName(), R.layout.notification_content_view);
        remoteView.setImageViewBitmap(R.id.image, bitmap);
        remoteView.setTextViewText(R.id.title, song.Title);
        remoteView.setTextViewText(R.id.additional, song.Author);
        remoteView.setImageViewResource(R.id.play_button, mEngine.isPlaying() ? R.drawable.ic_pause : R.drawable.ic_play);
        remoteView.setOnClickPendingIntent(R.id.play_button, PendingIntent.getBroadcast(this, 0,
                new Intent(NOTIFICATION_PLAY), PendingIntent.FLAG_UPDATE_CURRENT));
        remoteView.setOnClickPendingIntent(R.id.next_button, PendingIntent.getBroadcast(this, 0,
                new Intent(NOTIFICATION_NEXT), PendingIntent.FLAG_UPDATE_CURRENT));
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this).setSmallIcon(R.drawable.ic_play).setContentTitle(song.Title)
                .setOngoing(true).setLargeIcon(bitmap).setContentText(song.Author).setContent(remoteView);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    public void removeNotification() {
        mNotificationManager.cancel(NOTIFICATION_ID);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        Log.e(TAG, "onTaskRemoved = " + rootIntent);
        removeNotification();
    }
}
