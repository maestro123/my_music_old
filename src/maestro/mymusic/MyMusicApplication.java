package maestro.mymusic;

import android.app.Application;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import com.dream.android.mim.MIMDiskCache;
import com.msoft.android.service.MusicPlayerEngine;
import com.msoft.android.service.MusicPlayerService;
import maestro.lastfm.lib.data.LastFmDBHelper;
import maestro.mymusic.service.MyMusicService;
import maestro.mymusic.utils.SettingsHolder;
import maestro.mymusic.utils.Typefacer;

/**
 * Created by artyom on 7/11/14.
 */
public class MyMusicApplication extends Application {

    public static final String TAG = MyMusicApplication.class.getSimpleName();

    private MyMusicService musicPlayerService;

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            musicPlayerService = ((MyMusicService.LocalBinder) service).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            musicPlayerService = null;
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        new Typefacer(getApplicationContext());
        MIMDiskCache.getInstance().init(getCacheDir().getPath());
        MusicPlayerEngine.getInstance().initialize(getApplicationContext());
        LastFmDBHelper.getInstance().init(getApplicationContext());
        SettingsHolder.getInstance().init(getApplicationContext());

        Intent serviceStartIntent = new Intent(this, MyMusicService.class);
        startService(serviceStartIntent);
        bindService(serviceStartIntent, mConnection, BIND_AUTO_CREATE);

    }

    @Override
    public void onTerminate() {
        Log.e(TAG, "terminate");
        musicPlayerService.removeNotification();
        super.onTerminate();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        Log.e(TAG, "onTrimMemory - " + level);
    }

}
