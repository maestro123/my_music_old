package com.msoft.android.mplayer.service;
import com.msoft.android.mplayer.lib.models.Song;
interface IPlay{
	void play(int position);
	void play_pause();
	void next();
	void prev();
	void setSongs(in List<Song> songs);
	List<Song> getSongs();
	Song getCurrentSong();
	String[] get3Songs();
	boolean isPlaying();
	int getCurrentPlayingPosition();
}